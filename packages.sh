alts=(
  "bat"  # cat
  "colordiff"  # diff
  "dfc"  # df
  "exa"  # ls
  "fd"  # find
  "hexyl"  # hexdump
  "htop"  # top
  "mtr"  # tracepath
  "ncdu"  # du
  "neovim"  # vim
  "procs"  # ps
  "ripgrep"  # grep
  "sd"  # sed
)
alts_arch=(
  "${alts[@]}"
  "dstat"  # vmstat
)
alts_mac=(
  "${alts[@]}"
)
utils=(
  "curl"
  "git"
  "openssh"
  "starship"
  "tree"
  "tmux"
  "unzip"
  "wget"
  "vivid"  # LS_COLORS依存
)
utils_arch=(
  "${utils[@]}"
  "fakeroot"
  "pkgfile"
  "zsh"
)
utils_mac=(
  "${utils[@]}"
  "binutils"
  "coreutils"
  "diffutils"
  "findutils"
  "gawk"
  "gnupg"
  "gnu-sed"
  "gnu-tar"
  "grep"
  "gzip"
  "pinentry-mac"
)
devs=(
  "autoconf"
  "boost"
  "cmake"
  "ctags"
  "go"
  "gcc"
  "make"
  "npm"
)
devs_arch=(
  "${devs[@]}"
  "nodejs"
  "patch"
  "rustup"
  "rust-analyzer"
)
devs_mac=(
  "${devs[@]}"
  "direnv"
  "ghq"
  "node"
  "gpatch"
  "scc"
)

py3pkg=(
  "pipx"
)
py3bin=(
  "httpie"
  "pipenv"
  "poetry"
)

nodepkg=(
  "yarn"
  "npm-check-updates"
  "neovim"
  "pyright"
)