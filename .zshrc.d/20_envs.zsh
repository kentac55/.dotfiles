# *ENV

# pyenv
pyenv() {
  unfunction pyenv
  if command -v $PYENV_ROOT/bin/pyenv 1>/dev/null 2>&1; then
    export PATH="$PYENV_ROOT/bin:$PATH"
    eval "$(pyenv init -)"
    eval "$(pyenv virtualenv-init -)"

    ver=$(python --version)
    echo "PYENV LOADED($ver)"
    pyenv "$@"
  else
    echo "Failed to load pyenv"
  fi
}

## pipenv
pipenv() {
  unfunction pipenv
  eval "$(pipenv --completion)"
  pipenv "$@"
}

## rbenv
rbenv() {
  unfunction rbenv
  if command -v $RBENV_ROOT/bin/rbenv 1>/dev/null 2>&1; then
    export PATH="$RBENV_ROOT/bin:$PATH"
    eval "$(rbenv init - --no-rehash)"
    ver=$(ruby --version)
    echo "RBENV LOADED($ver)"
    rbenv "$@"
  else
    echo "Failed to load rbenv"
  fi
}

## nvm
nvm() {
  unfunction nvm
  if [ -f $NVM_DIR/nvm.sh ]; then
    . "$NVM_DIR/nvm.sh"
    # . "$NVM_DIR/bash_completion"
    echo "NVM LOADED(`node --version`)"
    nvm "$@"
  else
    echo "Failed to load nvm"
  fi
}

## direnv
if command -v direnv 1>/dev/null 2>&1; then
  eval "$(direnv hook zsh)"
fi
