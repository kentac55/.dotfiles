# setopt

# コメントの内容は全てsetopt時の挙動
# 不要な場合はunsetopt

# -----------------------------------------------
# Changing Directories
# ディレクトリ移動に関するいろいろ
# -----------------------------------------------

# コマンド名がディレクトリ時にcdする
setopt AUTO_CD
# cd時に自動的にpushdする
setopt AUTO_PUSHD
# cdの引数がディレクトリでなく / で始まらない場合、先頭に ~ を補完する
setopt CDABLE_VARS
# パスの一部に .. が含まれる場合、リンク元のディレクトリに解決する
# ex)$PWD=/foo/bar(ln /link/from)
# ex)setopt -> 'cd /foo/bar/..' -> '/link'
# ex)unsetopt -> 'cd /foo/bar/..' -> '/foo
unsetopt CHASE_DOTS
# シンボリックリンクをリンク元のパスに解決する
unsetopt CHASE_LINKS
# 'cd', 'chdir', 'pushd'をPOSIXなcdに変換する
unsetopt POSIX_CD
#同ディレクトリの複数のコピーをpushしない
setopt PUSHD_IGNORE_DUPS
# cdの+と-の引数を逆にする
setopt PUSHD_MINUS
# pushd, popdの度にディレクトリスタックの中身を表示しない
setopt PUSHD_SILENT
# 引数無しpushdは`pushd $HOME`にする
setopt PUSHD_TO_HOME

# -----------------------------------------------
# Completion
# 補完に関するいろいろ
# -----------------------------------------------

# カーソル位置を保持したまま補完を順次その場で表示する
setopt ALWAYS_LAST_PROMPT
# 補完時に文字列末尾へカーソル移動する
setopt ALWAYS_TO_END
# あいまい補完時に候補を表示する
setopt AUTO_LIST
# tab連打で補完候補を選択する
setopt AUTO_MENU
# ディレクトリの絶対パスがセットされた変数はそのディレクトリの名前になる
unsetopt AUTO_NAME_DIRS
# 括弧等補完する
setopt AUTO_PARAM_KEYS
# ディレクトリの末尾 / を補完する
setopt AUTO_PARAM_SLASH
# 末尾 / を自動削除する
setopt AUTO_REMOVE_SLASH
# あいまい補完の際、補完関数が二回呼ばれると自動で選択肢のリストを表示する
unsetopt BASH_AUTO_LIST
# 補完時、エイリアスを元のコマンドとは別のコマンドとみなす
unsetopt COMPLETE_ALIASES
# カーソル位置で補完する(通常末尾)
setopt COMPLETE_IN_WORD
# 現在の単語がglobパターンのとき、パターンにマッチしたものを生成し回す
setopt GLOB_COMPLETE
# 補完を行った際コマンドパスをハッシュする
unsetopt HASH_LIST_ALL
# 曖昧補完の際に、明確なprefixが挿入された場合候補を出すことなく補完する
setopt LIST_AMBIGUOUS
# 曖昧な補完の際、ビープ音を出す
unsetopt LIST_BEEP
# コンパクトな補完リストを表示する
setopt LIST_PACKED
# 補完リストを水平方向にソートする
unsetopt LIST_ROWS_FIRST
# 補完候補を種別表示する
setopt LIST_TYPES
# 曖昧な補完の際に、補完候補を表示するのではなく、最初にマッチしたものを挿入する
unsetopt MENU_COMPLETE
# 曖昧さがあっても入力と正確に一致する候補があればそれを優先する
unsetopt REC_EXACT

# -----------------------------------------------
# Expansion and Globbing
# 展開やグロブ(*とか)のいろいろ
# -----------------------------------------------

# ファイル名生成パターンがフォーマットにそぐわない場合、エラーを出力する
setopt BAD_PATTERN
# グロブパターンにて、末尾に'|', '(', '~'文字も含まないならば修飾子とみなす
setopt BARE_GLOB_QUAL
# {a-c} -> a b c
setopt BRACE_CCL
# ファイル名生成の際、大文字小文字を区別する
setopt CASE_GLOB
# zsh/regexモジュールにて、大文字小文字を区別する
setopt CASE_MATCH
# ファイル名生成パターンがマッチしないとき、引数リストからそのパターンを取り除く
unsetopt CSH_NULL_GLOB
# '=ls' -> '/bin/ls'
setopt EQUALS
# '#', '~', '^'をファイル指定パターンとする
# ex) '^readme' -> readme以外全て
# ex) '^*.js' -> jsファイル以外全て
# ex) '*~.js' -> jsファイル以外全て
# ex) '*.js~*.min* -> .minが含まれないjsファイル以外全て
# !!) 'git diff HEAD^' -> 'git diff HEAD\^'
setopt EXTENDED_GLOB
# 数値計算を浮動小数点演算する
unsetopt FORCE_FLOAT
# グロブを使用する
setopt GLOB
# 代入式の右辺で不明確なグロブ展開を使用する
unsetopt GLOB_ASSIGN
# dotfileを指定する際に先頭の'.'を入力不要にする
setopt GLOB_DOTS
# 変数の内容も展開に使用する
unsetopt GLOB_SUBST
# sや&の展開就職マッチングをパターンマッチにする
setopt HIST_SUBST_PATTERN
# ブレース展開を無効化する
# 歴史的背景によりこのオプションを有効化するとIGNORE_CLOSE_BRACESも有効化される
unsetopt IGNORE_BRACES
# コマンド最後の閉じ大カッコ'}'を不要にする(但し、最後のコマンドに;が無いとエラー)
unsetopt IGNORE_CLOSE_BRACES
# Ksh形式のグロブを使用する
unsetopt KSH_GLOB
# = 以降を補完する
# ex) '--prefix=/usr'
setopt MAGIC_EQUAL_SUBST
# glob展開時、ディレクトリ名の末尾に / を補完する
setopt MARK_DIRS
# マルチバイト文字を扱う
setopt MULTIBYTE
# ファイル生成パターンエラーを表示する
setopt MULTIBYTE
# ファイル生成パターンのエラーを表示しない
setopt NOMATCH
# エラー表示の代わりにパターンを除去する
unsetopt NULL_GLOB
# ファイル名生成を辞書順ではなく数値順にする
unsetopt NUMERIC_GLOB_SORT
# 展開に含まれる配列を別々に展開する
unsetopt RC_EXPAND_PARAM
# 正規表現マッチングでperlのアレ(PCRE)を使用する(=~と併用)
setopt REMATCH_PCRE
# kshやshのグロブを使用する
unsetopt SH_GLOB
# 定義されていない変数を空文字の変数として扱う(unsetの場合はエラー)
setopt UNSET
# 関数内の変数がグローバルとして定義されたら警告を出す
#setopt WARN_CREATE_GLOBAL
# ある関数内で定義されたローカル変数をその関数から呼ばれた関数によって上書きされる場合警告を出す(man読め)
#setopt WARN_NESTED_VAR

# ここから下はWIP

# History

# historyファイルを上書きではなく追記する
setopt APPEND_HISTORY
# !を使って履歴展開する
setopt BANG_HIST
# 履歴に実行時間を保存する
setopt EXTENDED_HISTORY
# 古い履歴を削除する場合、重複しているものから削除する
#setopt HIST_EXPIRE_DUPS_FIRST
# 入力したコマンドが直前のものと同一なら履歴に記録しない
setopt HIST_IGNORE_DUPS
# 先頭が' '(半角スペース)のコマンドは履歴に記録しない
setopt HIST_IGNORE_SPACE
# 関数定義のコマンドは履歴に記録しない
setopt HIST_NO_FUNCTIONS
# historyコマンドは履歴に記録しない
setopt HIST_NO_STORE
# コマンドの余分な空白を削除して履歴に記録する
setopt HIST_REDUCE_BLANKS
# 履歴をインクリメンタルに追加する
setopt INC_APPEND_HISTORY
# 履歴を共有する
setopt SHARE_HISTORY

# Initialisation

# Input/Output

# 自動でスペル修正を試みる
setopt CORRECT
# 全引数にスペル修正を試みる
setopt CORRECT_ALL
# <C-d>でログアウトしない
unsetopt IGNORE_EOF
# 対話モードでもコメントを有効にする
setopt INTERACTIVE_COMMENTS
# メール受信を通知する
setopt MAIL_WARNING
# / 付きのコマンド実行時、pathの中も走査する
# ex)$PATH=/usr/local/bin; 'X11/xinit'->'/usr/local/bin/X11/xinit'
setopt PATH_DIRS
# 8bit文字(日本語とか)表示する
setopt PRINT_EIGHT_BIT
# 返り値が0でなかった場合返り値を表示する
unsetopt PRINT_EXIT_VALUE

setopt auto_resume
setopt extended_history
setopt function_argzero
setopt list_packed
setopt list_types
setopt long_list_jobs
setopt no_beep
setopt no_clobber
setopt no_flow_control
setopt no_hup
setopt no_list_beep
setopt notify

setopt prompt_subst
