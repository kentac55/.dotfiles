autoload -Uz add-zsh-hook

function _load_python_version() {
  if [ -f .python-version ]; then
    pyenv version-file
    python --version
  fi
}

add-zsh-hook chpwd _load_python_version

function _load_ruby_version() {
  if [ -f .ruby-version ]; then
    rbenv version-file
    ruby --version
  fi
}

add-zsh-hook chpwd _load_ruby_version

function _load_node_version() {
  if [ -f .nvmrc ]; then
    nvm use
    node --version
  fi
}

add-zsh-hook chpwd _load_node_version
