if command -v kubectl 1>/dev/null 2>&1; then
  . <(kubectl completion zsh)
fi

if command -v minikube 1>/dev/null 2>&1; then
  . <(minikube completion zsh)
fi
