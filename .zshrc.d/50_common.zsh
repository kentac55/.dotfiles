# other settings

HISTFILE=~/.zhistory
HISTSIZE=1000000
SAVEHIST=1000000

function gittree {
  if [ -f ./.gitignore ]; then
    \tree --prune -I $(cat .gitignore ~/.gitignore | egrep -v "^#.*$|^[[:space:]]*$" | tr "\\n" "|")
  else
    \tree --prune -I $(cat ~/.gitignore | egrep -v "^#.*$|^[[:space:]]*$" | tr "\\n" "|")
  fi
}

function hex() {
  if [ $# -ne 1 ];then
    echo "target path required"
    return 1
  fi
  hexyl $1 | bat -p
}

function code-ext-dump() {
  setopt clobber
  code --list-extensions > $HOME/.dotfiles/XDG/Code/extensions
  setopt no_clobber
}
