zplug "zplug/zplug", hook-build:'zplug --self-manage'
# zplug "themes/ys", from:oh-my-zsh, defer:1
# https://cirw.in/blog/bracketed-paste
# zplug "plugins/safe-paste", from:oh-my-zsh
zplug "zsh-users/zsh-history-substring-search"
zplug "zsh-users/zsh-completions", use:"src/_*", lazy:true
zplug "zsh-users/zsh-syntax-highlighting", defer:2
zplug "modules/environment", from:prezto
zplug "modules/editor", from:prezto
zplug "modules/history", from:prezto
zplug "modules/directory", from:prezto
zplug "modules/spectrum", from:prezto
zplug "modules/utility", from:prezto
# zplug "modules/completion", from:prezto, on:"modules/utility", lazy:true
zplug "modules/git", from:prezto
zplug "modules/ssh", from:prezto
zplug "b4b4r07/enhancd", use:init.sh, defer:1
zplug "mollifier/cd-gitroot"
zplug "mrowa44/emojify", as:command
zplug "stedolan/jq", from:gh-r, as:command, rename-to:jq
zplug "docker/cli", use:"contrib/completion/zsh/_docker", lazy:true
# zplug "marlonrichert/zsh-autocomplete", defer:3
# zstyle ":autocomplete:tab:*" fzf-completion yes
# zstyle ':completion:*:complete:*:' tag-order \
#   '! ancestor-directories recent-directories recent-files' -
