function ghqfzf {
  local selected_dir=$(git config --get ghq.root)/$(ghq list --vcs git | fzf)
  if [ -n "selected_dir" ]; then
    BUFFER="cd ${selected_dir}"
    zle accept-line
  fi
}
zle -N ghqfzf
bindkey '^z' ghqfzf

function emojifzf {
  local emoji=${$(emojify -l | tail -n +5 | fzf):1}
  if [ -n "$DISPLAY" ]; then
    echo $emoji | xsel -bi
    BUFFER="echo ${emoji}"
  else
    BUFFER="echo ${emoji}"
  fi
  zle accept-line
}
zle -N emojifzf
bindkey '^s' emojifzf

function __cd_up() {
  BUFFER="\cd ../"
  zle accept-line
}
zle -N __cd_up
bindkey '^[[1;2A' __cd_up

function __cd_down() {
  BUFFER="\cd $OLDPWD"
  zle accept-line
}
zle -N __cd_down
bindkey '^[[1;2B' __cd_down

function __pushd() {
  BUFFER="\pushd"
  zle accept-line
}
zle -N __pushd
bindkey '^[[1;2D' __pushd

function __popd() {
  BUFFER="\popd"
  zle accept-line
}
zle -N __popd
bindkey '^[[1;2C' __popd
