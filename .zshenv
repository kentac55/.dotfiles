# zmodload zsh/zprof && zprof

if [ -z "$ZSH_ENV_LOADED" ]; then

  if [ -z "$X_MACHINE" ]; then
    uname=`uname -s`
    case $uname in
      Linux*)
        X_MACHINE="Linux"
        ;;
      Darwin*)
        X_MACHINE="Mac"
        ;;
      CYGWIN*)
        X_MACHINE="Cygwin"
        ;;
      MINGW*)
        X_MACHINE="MinGW"
        ;;
      *)
        X_MACHINE="UNKNOWN:${uname}"
        ;;
    esac
    export X_MACHINE
  fi

  if ([ -n "$TMUX" ] || [[ "$TTY" = /dev/pts/* ]] || [[ "$X_INSTALL" = "1" ]] || [ -n "$EDITOR_TERM" ]); then
    export CARGO_INSTALL_ROOT=$HOME
    export EDITOR="nvim"
    export ENHANCD_COMMAND=cd
    export ENHANCD_FILTER="fzf:fzf-tmux:peco"
    export FZF_DEFAULT_COMMAND='--files --hidden --glob "!.git"'
    export FZF_DEFAULT_OPTS='--height 40% --reverse --border'
    export CGO_ENABLED=0
    export GOPATH=$HOME
    export LESS="--tabs=4 --no-init --LONG-PROMPT --ignore-case --quit-if-one-screen --RAW-CONTROL-CHARS"
    export LESSCHARSET=utf-8
    # export LESS_TERMCAP_ZN=$(tput ssubm)
    # export LESS_TERMCAP_ZO=$(tput ssupm)
    # export LESS_TERMCAP_ZV=$(tput rsubm)
    # export LESS_TERMCAP_ZW=$(tput rsupm)
    # export LESS_TERMCAP_mb=$(tput bold; tput setaf 2)
    # export LESS_TERMCAP_md=$(tput bold; tput setaf 6)
    # export LESS_TERMCAP_me=$(tput sgr0)
    # export LESS_TERMCAP_mh=$(tput dim)
    # export LESS_TERMCAP_mr=$(tput rev)
    # export LESS_TERMCAP_se=$(tput rmso; tput sgr0)
    # export LESS_TERMCAP_so=$(tput bold; tput setaf 4; tput setab 3)
    # export LESS_TERMCAP_ue=$(tput rmul; tput sgr0)
    # export LESS_TERMCAP_us=$(tput smul; tput bold; tput setaf 7)
    export PAGER="less"
    export PYENV_ROOT="$HOME/.pyenv"
    export RBENV_ROOT="$HOME/.rbenv"
    export TFENV_ROOT="$HOME/.tfenv"
    export PATH="$HOME/bin:$TFENV_ROOT/bin:$PATH"
    export PIPX_HOME=$HOME
    export PIPX_BIN_DIR=$HOME/bin
    export COURSIER_BIN_DIR=$HOME/bin
    export PYTHONUSERBASE=$HOME
    if [ -n "$DISPLAY" ]; then
      export QT_QPA_PLATFORMTHEME=qt5ct
    fi
    export DOCKER_BUILDKIT=1
    # export DOCKER_CONTENT_TRUST=1
    if [ "$X_MACHINE" = "Mac" ]; then
      export DOCKER_HOST="unix:///var/run/docker.sock"
    else
      # rootless for linux
      export DOCKER_HOST="unix:///run/user/$UID/docker.sock"
    fi
    export GPG_TTY=$(tty)
    export ZPLUG_LOADFILE=~/.zshrc.d/zplug.zsh
  fi
fi

typeset -U path PATH
