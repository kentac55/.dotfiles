# dotz

dotfiles

## usage

### linux

1. git clone
1. `./install.sh -g`

### windows

```powershell
Set-ExecutionPolicy -Scope Process Unrestricted
iex(new-object net.webclient).downloadstring('https://gitlab.com/kentac55/.dotfiles/raw/master/make.ps1')
```