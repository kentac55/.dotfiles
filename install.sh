#!/bin/bash

# set -eux

cd $(dirname $0)
PROGNAME=$(basename $0)
DOTFILES=$PWD
XDG_CONFIG_BASE=$DOTFILES/.config
XDG_CONFIG_HOME=$HOME/.config

usage_exit() {
  echo "Usage: $PROGNAME [-h|-c|-g|-r]" 1>&2
  echo
  echo "Options:"
  echo "  -h  show this text"
  echo "  -c  for cli environment"
  echo "  -g  for gui environment(incl cli)"
  echo "  -r  for rdp environment(incl gui)"
  echo
  exit 1
}

if [ "$(id -u)" = "0" ]; then
  echo "must run under non-root user"
  exit 1
fi

MODE=""

while getopts cgrhx OPT
do
  case $OPT in
    h) usage_exit
      ;;
    \?) usage_exit
      ;;
    c) MODE="CLI"
      ;;
    g) MODE="GUI"
      ;;
    r) MODE="RDP"
      ;;
    x) MODE="CODE"
      ;;
  esac
done

echo $MODE

cli_install() {
  _cli_dep_install
  _cli_settings
}

_cli_dep_install() {

  mkdir -p $HOME/bin

  if [ ! -f /etc/pacman.conf.orig ]; then
    echo "create pacman.conf.orig"
    sudo cp /etc/pacman.conf /etc/pacman.conf.orig
  fi

  echo "check pacman.conf..."
  sudo sed -r \
           -e "s/#Color/Color/g" \
           -e "s/#VerbosePkgLists/VerbosePkgLists/g" \
           -e "s/#ParallelDownloads = 5/ParallelDownloads = 5/g" \
           -i /etc/pacman.conf

  echo "install required packages and update system-wide"
  source $PWD/packages.sh
  clidep=(
    "${alts_arch[@]}"
    "${utils_arch[@]}"
    "${devs_arch[@]}"
  )
  sudo pacman -Syuv --needed --noconfirm $(IFS=' '; echo "${clidep[*]}")

  if !(type yay > /dev/null 2>&1); then
    echo "install yay"
    if [ -d /tmp/yay ]; then
      cd /tmp/yay
      git pull
    else
      git clone --depth 1 https://aur.archlinux.org/yay.git /tmp/yay
      cd /tmp/yay
    fi

    PKGEXT='.pkg.tar.zstd' makepkg -srf
    yaypkg=$(find ./ | grep -P "^./yay-.*x86_64\.pkg.tar\.zstd$")
    sudo pacman -U $yaypkg --noconfirm
  fi
  cd $DOTFILES

  echo "install aur packages"
  cliaur=(
    "direnv"
    "ghq"
    "scc"
  )
  yay -S --needed --noconfirm $(IFS=' '; echo "${cliaur[*]}")

  echo "install diff-highlight"
  sudo ln -sf /usr/share/git/diff-highlight/diff-highlight /usr/local/bin/diff-highlight

  echo "install prettyping"
  rm -rf $HOME/bin/prettyping
  wget https://raw.githubusercontent.com/denilsonsa/prettyping/master/prettyping -O $HOME/bin/prettyping
  chmod +x $HOME/bin/prettyping

  echo "make bin dir..."
  cd $HOME
  mkdir -p $HOME/bin

  if !(type memo > /dev/null 2>&1); then
    echo "memo by k"
    $HOME/bin/ghq get -u github.com/kentac55/memo
    cd $HOME/src/github.com/kentac55/memo
    git checkout origin/modk
    GOPATH=$HOME GO111MODULE=on CGO_ENABLED=0 go install
  fi

  cd $DOTFILES

  echo "install rust bins..."
  rustup default stable
  cd $HOME

  echo "install gibo..."
  curl -L https://raw.github.com/simonwhitaker/gibo/master/gibo -so $HOME/bin/gibo
  chmod +x $HOME/bin/gibo
  gibo update
}

_cli_settings() {
  mkdir -p $HOME/.config
  ln -sf $DOTFILES/.editorconfig $HOME
  if [ -d $HOME/.fzf/.git ]; then
    cd $HOME/.fzf
    git pull
    cd $DOTFILES
  else
    rm -rf $HOME/.fzf
    git clone --depth 1 https://github.com/junegunn/fzf $HOME/.fzf
  fi
  $HOME/.fzf/install --all --xdg

  ln -sf $DOTFILES/.gitconfig $HOME
  ln -sf $DOTFILES/.gitconfig.identity.default $HOME
  ln -sf $DOTFILES/.gitignore $HOME
  ln -sf $DOTFILES/.tigrc $HOME
  ln -sf $DOTFILES/.gitmessage $HOME

  ln -sf $DOTFILES/.vimrc $HOME

  mkdir -p $XDG_CONFIG_HOME/nvim
  ln -sf $XDG_CONFIG_BASE/nvim/init.vim $XDG_CONFIG_HOME/nvim
  ln -sf $XDG_CONFIG_BASE/nvim/dein.toml $XDG_CONFIG_HOME/nvim
  ln -sf $XDG_CONFIG_BASE/nvim/dein_lazy.toml $XDG_CONFIG_HOME/nvim
  ln -sf $XDG_CONFIG_BASE/nvim/coc-settings.json $XDG_CONFIG_HOME/nvim

  if [ -d $HOME/.tmux/plugins/tpm/.git ]; then
    cd $HOME/.tmux/plugins/tpm
    git pull
    cd $DOTFILES
  else
    rm -rf $HOME/.tmux/plugins/tpm
    mkdir -p $HOME/.tmux/plugins
    git clone --depth 1 https://github.com/tmux-plugins/tpm $HOME/.tmux/plugins/tpm
  fi
  ln -sf $DOTFILES/.tmux.conf $HOME

  ln -sf $XDG_CONFIG_BASE/starship.toml $XDG_CONFIG_HOME/starship.toml

  ln -sf $DOTFILES/.zshrc $HOME
  ln -sf $DOTFILES/.zprofile $HOME
  ln -sf $DOTFILES/.zshenv $HOME
  ln -sf $DOTFILES/.zshrc.d $HOME
  if [ -d $HOME/.zplug/.git ]; then
    cd $HOME/.zplug
    git pull
    cd $DOTFILES
  else
    rm -rf $HOME/.zplug
    git clone --depth 1 https://github.com/zplug/zplug $HOME/.zplug
  fi
  X_INSTALL=1 zsh -c "source ~/.zshrc; zplug install; zplug update"

  if [ ! -d $HOME/src/github.com/kentac55/blog ]; then
    $HOME/bin/ghq get -u github.com/kentac55/blog
  fi

  mkdir -p $XDG_CONFIG_HOME/memo
  ln -sf $XDG_CONFIG_BASE/memo/config.toml $XDG_CONFIG_HOME/memo

  ln -sf $DOTFILES/.gemrc $HOME

  mkdir -p $HOME/.ssh
  ln -sf $DOTFILES/.ssh/config.linux $HOME/.ssh/config

  echo "prepare pyenv..."
  if [ -d $HOME/.pyenv/.git ]; then
    cd $HOME/.pyenv
    git pull
    cd $DOTFILES
  else
    rm -rf $HOME/.pyenv
    git clone --depth 1 https://github.com/pyenv/pyenv.git $HOME/.pyenv
  fi

  echo "prepare pyenv-virtualenv"
  if [ -d $HOME/.pyenv/plugins/pyenv-virtualenv/.git ]; then
    cd $HOME/.pyenv/plugins/pyenv-virtualenv
    git pull
    cd $DOTFILES
  else
    rm -rf $HOME/.pyenv/plugins/pyenv-virtualenv
    git clone --depth 1 https://github.com/pyenv/pyenv-virtualenv.git $HOME/.pyenv/plugins/pyenv-virtualenv
  fi

  echo "fetch python version"
  export PYENV_ROOT="$HOME/.pyenv"
  eval "$($HOME/.pyenv/bin/pyenv init -)"
  pyver3=$($HOME/.pyenv/bin/pyenv install --list | grep -P "^  3.\d+.\d+$" | tail -n 1 | sed -r -e "s/  //g")

  echo "install python $pyver3"
  $HOME/.pyenv/bin/pyenv install -s $pyver3
  $HOME/.pyenv/bin/pyenv global $pyver3

  $HOME/.pyenv/bin/pyenv virtualenv $pyver3 py3nvim
  $HOME/.pyenv/bin/pyenv activate py3nvim
  pip install pynvim
  $HOME/.pyenv/bin/pyenv deactivate

  # $HOME/.pyenv/bin/pyenv virtualenv $pyver2 py2nvim
  # $HOME/.pyenv/bin/pyenv activate py2nvim
  # pip install pynvim
  # $HOME/.pyenv/bin/pyenv deactivate
  #
  # $HOME/.pyenv/bin/pyenv virtualenv $pyver3 py3nvim
  # $HOME/.pyenv/bin/pyenv activate py3nvim
  # pip install pynvim
  # $HOME/.pyenv/bin/pyenv deactivate

  echo "prepare rbenv..."
  export RBENV_ROOT="$HOME/.rbenv"
  if [ -d $HOME/.rbenv/.git ]; then
    cd $HOME/.rbenv
    git pull
    cd $DOTFILES
  else
    rm -rf $HOME/.rbenv
    git clone --depth 1 https://github.com/rbenv/rbenv.git $HOME/.rbenv
  fi
  if [ -d $HOME/.rbenv/plugins/ruby-build/.git ]; then
    cd $HOME/.rbenv/plugins/ruby-build
    git pull
    cd $DOTFILES
  else
    rm -rf $HOME/.rbenv/plugins/ruby-build
    mkdir -p $HOME/.rbenv/plugins
    git clone --depth 1 https://github.com/rbenv/ruby-build.git $HOME/.rbenv/plugins/ruby-build
  fi
  eval "$($HOME/.rbenv/bin/rbenv init - --no-rehash)"
  rbver=$($HOME/.rbenv/bin/rbenv install --list | grep -P "^3.\d+.\d+$" | tail -n 1)

  echo "install ruby $rbver"
  $HOME/.rbenv/bin/rbenv install -s $rbver
  # $HOME/.rbenv/bin/rbenv global $rbver

  npm config set prefix=$HOME

  echo "prepare nvm..."
  if [ -d $HOME/.nvm/.git ]; then
    cd $HOME/.nvm
    git pull
    cd $DOTFILES
  else
    rm -rf $HOME/.nvm
    git clone --depth 1 https://github.com/creationix/nvm.git $HOME/.nvm
  fi
  . "$HOME/.nvm/nvm.sh"
  echo "install latest node"
  nvm install --lts
  # nvm use --lts
  nvm unload

  echo "prepare tfenv"
  export TFENV_ROOT="$HOME/.tfenv"
  if [ -d $HOME/.tfenv/.git ]; then
    cd $HOME/.tfenv
    git pull
    cd $DOTFILES
  else
    rm -rf $HOME/.tfenv
    git clone --depth 1 https://github.com/tfutils/tfenv.git ~/.tfenv
  fi
  echo "install latest terraform"
  PATH="$HOME/.tfenv/bin:$PATH" $HOME/.tfenv/bin/tfenv install latest
  PATH="$HOME/.tfenv/bin:$PATH" $HOME/.tfenv/bin/tfenv use latest

  echo "install python3 packages"

  X_INSTALL=1 zsh -c "source ~/.zshrc; pyenv virtualenv $pyver3 py3nvim; pyenv activate py3nvim; pip install pynvim"

  # $HOME/.pyenv/shims/pip install $(IFS=' '; echo "${py3pkg[*]}") --upgrade
  PYTHONUSERBASE=$HOME /usr/bin/python3 -m pip install --user --upgrade $(IFS=' '; echo "${py3pkg[*]}")

  for pkg in ${py3bin[@]}; do
    PIPX_HOME=$HOME $HOME/bin/pipx install --force $pkg
  done
  PIPX_HOME=$HOME $HOME/bin/pipx reinstall-all
  # pipx upgrade-all

  # echo "install ruby packages"
  # rbpkg=(
  #   "neovim"
  # )
  # $HOME/.rbenv/shims/gem install $(IFS=' '; echo "${rbpkg[*]}")
  # $HOME/.rbenv/shims/gem update
  # gem install $(IFS=' '; echo "${rbpkg[*]}")
  # gem update

  echo "install node packages"
  npm install -g $(IFS=' '; echo "${nodepkg[*]}")
  npm update -g

  echo "place gnupg configs"
  mkdir -p $HOME/.gnupg
  ln -sf $DOTFILES/.gnupg/gpg.conf $HOME/.gnupg
  ln -sf $DOTFILES/.gnupg/gpg-agent.conf $HOME/.gnupg
}

gui_install() {
  cli_install
  _gui_dep_install
  _gui_settings
}

code_settings() {
  cmds="code code-oss code-insiders vscode"
  for cmd in ${cmds[@]}; do
    if type $cmd > /dev/null 2>&1; then
      _code_ext "$cmd"
    fi
  done
  _code_cfg
}

_code_ext() {
  echo "install extensions to $cmd"
  \cat $XDG_CONFIG_BASE/Code/extensions | while read line; do
    echo "add ${line} to ${cmd}"
    $cmd --install-extension $line --force
  done
  echo "uninstall extensions to $cmd"
  \cat $XDG_CONFIG_BASE/Code/x_extensions | while read line; do
    echo "remove ${line} from ${cmd}"
    $cmd --uninstall-extension $line --force
  done
}

_code_cfg() {
  echo "place configs"
  mkdir -p $XDG_CONFIG_HOME/Code/User
  mkdir -p $XDG_CONFIG_HOME/Code\ -\ OSS/User
  mkdir -p $XDG_CONFIG_HOME/Code\ -\ Insiders/User
  ln -sf $XDG_CONFIG_BASE/Code/User/settings.json $XDG_CONFIG_HOME/Code/User
  ln -sf $XDG_CONFIG_BASE/Code/User/keybindings.json $XDG_CONFIG_HOME/Code/User
  ln -sf $XDG_CONFIG_BASE/Code/User/settings.json $XDG_CONFIG_HOME/Code\ -\ OSS/User
  ln -sf $XDG_CONFIG_BASE/Code/User/keybindings.json $XDG_CONFIG_HOME/Code\ -\ OSS/User
  ln -sf $XDG_CONFIG_BASE/Code/User/settings.json $XDG_CONFIG_HOME/Code\ -\ Insiders/User
  ln -sf $XDG_CONFIG_BASE/Code/User/keybindings.json $XDG_CONFIG_HOME/Code\ -\ Insiders/User
}

_gui_dep_install() {
  guidep=(
    "awesome"
    "dmenu"
    "fcitx-configtool"
    "fcitx-im"
    "fcitx-mozc"
    "firefox-developer-edition"
    "gnome-keyring"
    "libgnome-keyring"
    "graphicsmagick"
    "mpc"
    "mpd"
    "noto-fonts"
    "pavucontrol"
    "pulseaudio-alsa"
    "qt5ct"
    "sakura"
    "scrot"
    "slock"
    "sxiv"
    "xbindkeys"
    "xcompmgr"
    "xorg-server"
    "xorg-xinit"
    "xorg-xmodmap"
    "xorg-xprop"
    "xorg-xrandr"
    "xsel"
  )
  guiaur=(
    "microsoft-edge-dev-bin"
    "visual-studio-code-insiders-bin"
    "noto-fonts-emoji-blob"
  )
  yay -S --needed --noconfirm $(IFS=' '; echo "${guidep[*]}")
  yay -S --needed --noconfirm $(IFS=' '; echo "${guiaur[*]}")
}

_gui_settings() {
  echo "place .xinitrc"
  ln -sf $DOTFILES/.xinitrc $HOME

  echo "place .xbindkeysrc"
  ln -sf $DOTFILES/.xbindkeysrc $HOME

  echo "place .Xmodmap"
  ln -sf $DOTFILES/.Xmodmap $HOME

  echo "place awesome configs"
  mkdir -p $HOME/.cache/awesome
  if [ -d $XDG_CONFIG_HOME/awesome/.git ]; then
    cd $XDG_CONFIG_HOME/awesome
    git pull
    git submodule update
  else
    rm -rf $XDG_CONFIG_HOME/awesome
    git clone --depth 1 https://github.com/lcpz/awesome-copycats.git $XDG_CONFIG_HOME/awesome
    cd $XDG_CONFIG_HOME/awesome
    sed -r -e 's/git:/https:/g' -i .gitmodules
    git submodule init
    git submodule sync
    git submodule update
  fi
  cd $DOTFILES
  ln -sf $XDG_CONFIG_BASE/awesome/rc.lua $XDG_CONFIG_HOME/awesome/rc.lua
  ln -sf $XDG_CONFIG_BASE/awesome/theme-personal.lua $XDG_CONFIG_HOME/awesome/themes/powerarrow-dark/theme-personal.lua
  ln -sf $DOTFILES/resources/miki.jpg $XDG_CONFIG_HOME/awesome/themes/powerarrow-dark/miki.jpg

  echo "place .gtkrc-2.0"
  ln -sf $DOTFILES/.gtkrc-2.0 $HOME/.gtkrc-2.0

  echo "place gtk-3.0"
  mkdir -p $XDG_CONFIG_HOME/gtk-3.0
  ln -sf $XDG_CONFIG_BASE/gtk-3.0/settings.ini $XDG_CONFIG_HOME/gtk-3.0/settings.ini

  echo "place .ideavimrc"
  ln -sf $DOTFILES/.ideavimrc $HOME/.ideavimrc

  echo "place alacritty config"
  mkdir -p $XDG_CONFIG_HOME/alacritty
  ln -sf $XDG_CONFIG_BASE/alacritty/alacritty.yml $XDG_CONFIG_HOME/alacritty

  # echo "place sakura config"
  # mkdir -p $XDG_CONFIG_HOME/sakura
  # ln -sf $XDG/sakura/sakura.conf $XDG_CONFIG_HOME/sakura

  echo "modify font config"
  mkdir -p $HOME/.local/share/fonts
  mkdir -p $XDG_CONFIG_HOME/fontconfig/conf.d
  # ln -sf $DOTFILES/resources/HackGenConsole-Regular-forPowerline-patched.ttf $HOME/.local/share/fonts
  # ln -sf $DOTFILES/resources/HackGenConsole-Bold-forPowerline-patched.ttf $HOME/.local/share/fonts
  ln -sf $XDG_CONFIG_BASE/fontconfig/conf.d/50-blobmoji.conf $XDG_CONFIG_HOME/fontconfig/conf.d
  fc-cache -vf

  if [ ! -f ~/.local/share/fonts/Cica-Regular.ttf ]; then
    download_cica
  fi

  echo "place qtconfig"
  mkdir -p $XDG_CONFIG_HOME/qt5ct
  ln -sf $XDG_CONFIG_BASE/Trolltech.conf $XDG_CONFIG_HOME
  ln -sf $XDG_CONFIG_BASE/qt5ct/qt5ct.conf $XDG_CONFIG_HOME/qt5ct

  echo "create screenshot folder"
  mkdir -p $HOME/screenshots

  echo "compile libsecret helper"
  cd /usr/share/git/credential/gnome-keyring
  sudo make
  cd -

  code_settings
}

download_cica() {
  mkdir -p /tmp/cica
  cd /tmp/cica
  wget https://github.com/miiton/Cica/releases/download/v5.0.2/Cica_v5.0.2_without_emoji.zip
  unzip *.zip
  mv *.ttf ~/.local/share/fonts
  cd -
}

rdp_install() {
  gui_install
  _rdp_dep_install
}

_rdp_dep_install() {
  echo "install rdp deps"
  rdpdep=(
    "xorgxrdp-git"
    "xrdp-git"
  )
  yay -S --needed --noconfirm $(IFS=' '; echo "${rdpdep[*]}")
}

if [ "$MODE" = "CLI" ]; then
  cli_install
elif [ "$MODE" = "GUI" ]; then
  gui_install
elif [ "$MODE" = "RDP" ]; then
  rdp_install
elif [ "$MODE" = "CODE" ]; then
  code_settings
else
  usage_exit
fi
