set encoding=utf-8

set hidden
set cmdheight=2
set updatetime=300
set shortmess+=c
set signcolumn=yes

set clipboard+=unnamedplus

" highlight
syntax enable
set showmatch
set matchtime=3
set cursorline

" encoding
set fileencoding=utf-8
set fileencodings=ucs-boms,utf-8,euc-jp,cp932
set fileformats=unix,dos,mac

" there is no answer...
set ambiwidth=single

" statusline
set laststatus=2
set showmode
set showcmd
set ruler
set wildmenu
set history=5000

" indent
set expandtab
set tabstop=2
set softtabstop=2
set autoindent
set smartindent
set shiftwidth=2
set foldmethod=indent

" search
set incsearch
set ignorecase
set smartcase
set hlsearch
set isk-=_
nnoremap <silent><Esc><Esc> :<C-u>set nohlsearch!<CR><C-l>

" cursor
set whichwrap=b,s,h,l,<,>,[,],~
set number
let &t_SI="\<CSI>5 q"
let &t_EI="\<CSI>1 q"

" scroll
set scrolloff=5

" special chars
set list
set listchars=tab:»-,trail:-,eol:↲,extends:»,precedes:«,nbsp:%

" remap
nnoremap <Space> <Nop>
let mapleader = "\<Space>"
nnoremap j gj
nnoremap k gk
nnoremap <down> gj
nnoremap <up> gk
nnoremap Q <Nop>
nnoremap ; :
nnoremap : ;
vnoremap ; :
vnoremap : ;
nnoremap <Leader>t :<C-u>vs<CR><C-w>l:term<CR>i
nnoremap <Leader>T :<C-u>term<CR>i
tnoremap <silent> jj <C-\><C-n>
nnoremap x "_x
vnoremap x "_x

" SUMMON GOD OF VIM
inoremap <silent> jj <ESC>
cnoremap w!! w !sudo tee > /dev/null %<CR> :e!<CR>

"set viminfo=

set backspace=indent,eol,start

" unlimited undo
if has('persistent_undo')
  set undodir=~/.config/nvim/undo
  set undofile
endif

if has('autocmd')
  augroup vimrc
    autocmd!
    " color fix
    autocmd ColorScheme * highlight Normal ctermbg=none
    autocmd ColorScheme * highlight LineNr ctermbg=none
    " close window automaticaly when the last window is quickfix
    autocmd BufWinEnter quickfix nnoremap <silent> <buffer> q :cclose<cr>:lclose<cr>
    autocmd BufEnter * if (winnr('$') == 1 && &buftype ==# 'quickfix' ) | bd | q | endif
    autocmd BufReadPost *
    \ if line("'\"") > 0 && line("'\"") <= line('$') |
    \   exe "normal! g`\"" |
    \ endif
    " json with comments
    autocmd FileType json syntax match Comment +\/\/.\+$+
  augroup END
endif

set background=dark
filetype plugin indent on

" その場の単語をhighlight
nnoremap <silent> <Leader>f "zyiw:let @/ = '\<' . @z . '\>'<CR>:set hlsearch<CR>
" その場の単語を一括replace
nnoremap <Leader>r "zyiw:let @/ = '\<' . @z . '\>'<CR>:set hlsearch<CR>:%s/<C-r>///g<Left><Left>
" 行を移動
nnoremap K "zdd<Up>"zP
nnoremap J "zdd"zp
" 複数行を移動
vnoremap K "zx<Up>"zP`[V`]
vnoremap J "zx"zp`[V`]
" acb to abc
inoremap <C-t> <Esc><Left>"zx"zpa

" wrap
nnoremap s" ciw""<Esc>P
nnoremap s' ciw''<Esc>P
nnoremap s` ciw``<Esc>P
nnoremap s( ciw()<Esc>P
nnoremap s{ ciw{}<Esc>P
nnoremap s[ ciw[]<Esc>P
nnoremap s< ciw<><Esc>P