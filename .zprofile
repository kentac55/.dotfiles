# MacによるPATH上書き回避
if [ "$X_MACHINE" = "Mac" ]; then
  sed=gsed
else
  sed=sed
fi

export LS_COLORS="$(vivid generate molokai)"
export NVM_DIR="$(realpath $HOME/.nvm)"
if [ -n $PYENV_ROOT ] && [ -n $RBENV_ROOT ] && [ -f $RBENV_ROOT/shims/gem ]; then
  export PATH="$PYENV_ROOT/shims:`$RBENV_ROOT/shims/gem environment gempath | $sed -r -e "s/:/\/bin:/" -e "s/$/\/bin/"`:$PATH"
fi
