vim.g.mapleader = ' '

require('plugins')
require('lsp')

vim.g.loaded_ruby_provider = false
vim.g.loaded_perl_provider = false

if vim.fn.has('win64') == 1 then
  vim.api.nvim_set_var('vimproc#dll_path', '$VIMRUNTIME/lib/vimproc_win64.dll')
  vim.api.nvim_set_var('vimproc#download_windows_dll', 1)
elseif vim.fn.has('mac') == 1 then
  vim.g.python_host_prog = vim.env.PYENV_ROOT .. '/versions/py2nvim/bin/python'
  vim.g.python3_host_prog = '/usr/local/bin/python3.9'
  vim.g.node_default_path = '/usr/bin/node'
else
  vim.g.python_host_prog = vim.env.PYENV_ROOT .. '/versions/py2nvim/bin/python'
  vim.g.python3_host_prog = vim.env.PYENV_ROOT .. '/versions/py3nvim/bin/python'
  vim.g.node_default_path = '/usr/bin/node'
end

vim.o.encoding = 'utf-8'

vim.o.hidden = true
vim.o.cmdheight = 2
vim.o.updatetime = 300
vim.o.shortmess = vim.o.shortmess .. 'c'
vim.o.signcolumn = 'yes'

vim.o.clipboard = 'unnamedplus'

-- highlight
-- syntax enable
vim.o.showmatch = true
vim.o.matchtime = 3
vim.o.cursorline = true

-- encoding
vim.o.fileencoding = 'utf-8'
vim.o.fileencodings = 'ucs-boms,utf-8,euc-jp,cp932'
vim.o.fileformats = 'unix,dos,mac'

vim.o.ambiwidth = 'single'

-- statusline
vim.o.laststatus = 2
vim.o.showmode = true
vim.o.showcmd = true
vim.o.ruler = true
vim.o.wildmenu = true
vim.o.history = 5000

-- indent
vim.o.expandtab = true
vim.o.tabstop = 2
vim.o.softtabstop = 2
vim.o.autoindent = true
vim.o.smartindent = true
vim.o.shiftwidth = 2
-- vim.o.foldmethod = indent

-- search
vim.o.incsearch = true
vim.o.ignorecase = true
vim.o.smartcase = true
vim.o.hlsearch = true

vim.api.nvim_set_keymap('n', '<Esc><Esc>', ':<C-u>set nohlsearch!<CR><C-l>', { noremap = true, silent = true })


-- cursor
vim.o.whichwrap = 'b,s,h,l,<,>,[,],~'
vim.o.number = true

-- scroll
vim.o.scrolloff = 5

-- special chars
vim.o.list = true
vim.o.listchars = 'tab:»-,trail:-,eol:↲,extends:»,precedes:«,nbsp:%'

-- terminal
function _G.w_term()
  if vim.fn.has('win64') == 1 then
    vim.cmd('terminal pwsh')
  else
    vim.cmd('terminal')
  end
end


-- remap
vim.api.nvim_set_keymap('n', ' ', '<Nop>', { noremap = true, silent = true })
vim.api.nvim_set_keymap('n', 'j', 'gj', { noremap = true, silent = true })
vim.api.nvim_set_keymap('n', 'k', 'gk', { noremap = true, silent = true })
vim.api.nvim_set_keymap('n', '<down>', 'gj', { noremap = true, silent = true })
vim.api.nvim_set_keymap('n', '<up>', 'gk', { noremap = true, silent = true })
vim.api.nvim_set_keymap('n', '<up>', 'gk', { noremap = true, silent = true })
vim.api.nvim_set_keymap('n', 'Q', '<Nop>', { noremap = true, silent = true })
vim.api.nvim_set_keymap('n', ';', ':', { noremap = true, silent = false })
vim.api.nvim_set_keymap('n', ':', ';', { noremap = true, silent = false })
vim.api.nvim_set_keymap('v', ';', ':', { noremap = true, silent = false })
vim.api.nvim_set_keymap('v', ':', ';', { noremap = true, silent = false })
vim.api.nvim_set_keymap('n', '<Leader>t', ':<C-u>vs<CR><C-w>l:lua w_term()<CR>', { noremap = true, silent = false })
vim.api.nvim_set_keymap('n', '<Leader>T', ':<C-u>lua w_term()<CR>', { noremap = true, silent = false })
vim.api.nvim_set_keymap('t', 'jj', '<C-\\><C-n>', { noremap = true, silent = true })
vim.api.nvim_set_keymap('n', 'x', '"_x', { noremap = true, silent = true })

-- remap-pane/window
vim.api.nvim_set_keymap('n', '<C-w>m', '<C-w>_<C-w>\\|', { noremap = true, silent = false })

vim.api.nvim_set_keymap('i', 'jj', '<ESC>', { noremap = true, silent = true })
vim.api.nvim_set_keymap('c', 'w!!', 'w !sudo tee > /dev/null %<CR> :e!<CR>', { noremap = true, silent = false })

vim.o.backspace = 'indent,eol,start'

vim.o.undofile = true

vim.cmd([[
function! IsEndWithSemicolon()
  let c = getline(".")[col("$")-2]
  if c != ";"
    return 1
  else
    return 0
  endif
endfunction


augroup vimrc
  autocmd!
  " close window automaticaly when the last window is quickfix
  autocmd BufWinEnter quickfix nnoremap <silent> <buffer> q :cclose<cr>:lclose<cr>
  autocmd BufEnter * if (winnr('$') == 1 && &buftype ==# 'quickfix' ) | bd | q | endif
  autocmd BufReadPost *
  \ if line("'\"") > 0 && line("'\"") <= line('$') |
  \   exe "normal! g`\"" |
  \ endif
  " json with comments
  autocmd FileType json syntax match Comment +\/\/.\+$+
  " Ctrl + Enter
  autocmd FileType rust inoremap <expr>; IsEndWithSemicolon() ? "<C-o>$;<CR>" : "<C-o>$<CR>"
  autocmd TermOpen * startinsert
augroup END
]])


vim.o.background = 'dark'

-- https://qiita.com/itmammoth/items/312246b4b7688875d023
-- その場の単語をhighlight
vim.api.nvim_set_keymap('n', '<Leader>f', '"zyiw:let @/ = \'\\<\' . @z . \'\\>\'<CR>:set hlsearch<CR>', { noremap = true, silent = true })
-- その場の単語を一括replace
vim.api.nvim_set_keymap('n', '<Leader>r', '"zyiw:let @/ = \'\\<\' . @z . \'\\>\'<CR>:set hlsearch<CR>:%s/<C-r>///g<Left><Left>', { noremap = true, silent = true })
-- 行を移動
vim.api.nvim_set_keymap('n', 'K', '"zddk"zP', { noremap = true, silent = false })
vim.api.nvim_set_keymap('n', 'J', '"zdd"zp', { noremap = true, silent = false })
-- 複数行を移動
vim.api.nvim_set_keymap('v', 'K', '"zxk"zP`[V`]', { noremap = true, silent = false })
vim.api.nvim_set_keymap('v', 'J', '"zx"zp`[V`]', { noremap = true, silent = false })
-- acb to abc
vim.api.nvim_set_keymap('i', '<C-t>', '<Esc><Left>"zx"zpa', { noremap = true, silent = false })

-- wrap
vim.api.nvim_set_keymap('n', 's"', 'ciw""<Esc>P', { noremap = true, silent = false })
vim.api.nvim_set_keymap('n', 's\'', 'ciw\'\'<Esc>P', { noremap = true, silent = false })
vim.api.nvim_set_keymap('n', 's`', 'ciw``<Esc>P', { noremap = true, silent = false })
vim.api.nvim_set_keymap('n', 's(', 'ciw()<Esc>P', { noremap = true, silent = false })
vim.api.nvim_set_keymap('n', 's{', 'ciw{}<Esc>P', { noremap = true, silent = false })
vim.api.nvim_set_keymap('n', 's[', 'ciw[]<Esc>P', { noremap = true, silent = false })
vim.api.nvim_set_keymap('n', 's<', 'ciw<><Esc>P', { noremap = true, silent = false })

vim.opt.completeopt = "menu,menuone,noselect"


--[[ local cmp_autopairs = require('nvim-autopairs.completion.cmp') ]]
local cmp = require"cmp"
cmp.setup({
  snippet = {
    expand = function(args)
      vim.fn["vsnip#anonymous"](args.body)
    end,
  },
  mapping = {
    ["<C-d>"] = cmp.mapping(cmp.mapping.scroll_docs(-4), { 'i', 'c' }),
    ["<C-f>"] = cmp.mapping(cmp.mapping.scroll_docs(4), { 'i', 'c' }),
    ["<C-CR>"] = cmp.mapping(cmp.mapping.complete(),  { 'i', 'c' }),
    ['<C-e>'] = cmp.mapping({
      i = cmp.mapping.abort(),
      c = cmp.mapping.close(),
    }),
    ["<CR>"] = cmp.mapping.confirm({ select = true }),
    ['<C-n>'] = cmp.mapping(cmp.mapping.select_next_item(), { 'i', 's' }),
    ['<C-p>'] = cmp.mapping(cmp.mapping.select_prev_item(), { 'i', 's' }),
    ['<Tab>'] = cmp.mapping(cmp.mapping.select_next_item(), { 'i', 's' }),
    ['<S-Tab>'] = cmp.mapping(cmp.mapping.select_prev_item(), { 'i', 's' }),
  },
  sources = cmp.config.sources({
    { name = "nvim_lsp" },
    { name = "vsnip" },
  }, {
    { name = "buffer" },
  })
})
--[[ cmp.event:on( 'confirm_done', cmp_autopairs.on_confirm_done({  map_char = { tex = '' } })) ]]
--[[ cmp_autopairs.lisp[#cmp_autopairs.lisp+1] = "racket" ]]

cmp.setup.cmdline('/', {
  sources = {
    { name = 'buffer' }
  }
})

cmp.setup.cmdline(':', {
  sources = cmp.config.sources({
    { name = 'path' }
  }, {
    { name = 'cmdline' }
  })
})

package.loaded['colors'] = {
  ['darcula'] = require('colors.darcula'),
}

vim.cmd('colorscheme base16-monokai')
vim.opt_global.shortmess:remove("F")
vim.opt_global.isk:remove("_")


local function t(str)
  return vim.api.nvim_replace_termcodes(str, true, true, true)
end

function _G.smart_tab()
  return vim.fn.pumvisible() == 1 and t'<C-n>' or t'<Tab>'
end

vim.api.nvim_set_keymap('i', '<Tab>', 'v:lua.smart_tab()', {expr = true, noremap = true})
