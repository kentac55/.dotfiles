local keymap = function(mode, lhs, rhs)
    vim.api.nvim_set_keymap(mode, lhs, rhs, { noremap = true, silent = false, })
end
local fn = vim.fn
local install_path = fn.stdpath('data')..'/site/pack/packer/start/packer.nvim'
if fn.empty(fn.glob(install_path)) > 0 then
  Packer_bootstrap = fn.system({'git', 'clone', '--depth', '1', 'https://github.com/wbthomason/packer.nvim', install_path})
end

vim.cmd([[
  augroup packer_user_config
    autocmd!
    autocmd BufWritePost plugins.lua source <afile> | PackerCompile
  augroup end
]])

vim.cmd [[packadd packer.nvim]]

return require('packer').startup(function(use)
  -- Packer can manage itself
  use 'wbthomason/packer.nvim'
  use 'neovim/nvim-lspconfig'
  use 'williamboman/nvim-lsp-installer'

  -- Simple plugins can be specified as strings
  --[[ use '9mm/vim-closer' ]]
  --[[ use {'windwp/nvim-autopairs', config = function() require('nvim-autopairs').setup{} end} ]]

  -- Lazy loading:
  -- Load on specific commands
  -- テスト実行するときに勝手に分割してくれるやつ
  use {
    'tpope/vim-dispatch',
    opt = true,
    cmd = {'Dispatch', 'Make', 'Focus', 'Start'},
    disable = vim.fn.has('win64') == 1,
  }

  -- Load on an autocommand event
  -- %で反対側に飛ぶ機能を強化してくれる
  use {'andymass/vim-matchup', event = 'VimEnter'}

  -- Load on a combination of conditions: specific filetypes or commands
  -- Also run code after load (see the "config" key)
  -- いつもの
  use {
    'w0rp/ale',
    ft = {'sh', 'zsh', 'bash', 'c', 'cpp', 'cmake', 'html', 'markdown', 'racket', 'vim', 'tex', 'go'},
    cmd = 'ALEEnable',
    config = function()
      vim.cmd [[ALEEnable]]
    end,
  }

  -- markdownをbrowserで表示してくれるらしい？
  use { 'iamcco/markdown-preview.nvim', run = 'cd app && yarn install', cmd = 'MarkdownPreview' }

  -- パーサー
  use {
    'nvim-treesitter/nvim-treesitter',
    run = ':TSUpdate',
    --[[ disable = vim.fn.has('win64') == 1, ]]
    config = function ()
      require('nvim-treesitter.configs').setup {
        -- One of "all", "maintained" (parsers with maintainers), or a list of languages
        -- ensure_installed = "all",

        -- Install languages synchronously (only applied to `ensure_installed`)
        sync_install = false,

        -- List of parsers to ignore installing
        ignore_install = { "javascript" },

        highlight = {
          -- `false` will disable the whole extension
          enable = true,

          -- list of language that will be disabled
          disable = { "c", "rust" },

          -- Setting this to true will run `:h syntax` and tree-sitter at the same time.
          -- Set this to `true` if you depend on 'syntax' being enabled (like for indentation).
          -- Using this option may slow down your editor, and you may see some duplicate highlights.
          -- Instead of true it can also be a list of languages
          additional_vim_regex_highlighting = false,
        },
      }
    end,
  }

  -- Post-install/update hook with call of vimscript function with argument
  -- chrome/firefoxでnvimが動く・・・はず？(動かない)
  use { 'glacambre/firenvim', run = function() vim.fn['firenvim#install'](0) end }

  -- Use specific branch, dependency and run lua file after load
  -- statuslineっぽいやつ
  use {
    'NTBBloodbath/galaxyline.nvim',
    branch = 'main',
    config = function() require'spaceline' end,
    requires = {'kyazdani42/nvim-web-devicons'}
  }

  -- Use dependency and run lua function after load
  -- 左のサイン
  use {
    'lewis6991/gitsigns.nvim', requires = { 'nvim-lua/plenary.nvim' },
    config = function() require('gitsigns').setup() end,
  }

  -- You can specify multiple plugins in a single call
  -- use {'tjdevries/colorbuddy.vim', {'nvim-treesitter/nvim-treesitter', opt = true}}

  use "hrsh7th/cmp-nvim-lsp"
  use "hrsh7th/cmp-buffer"
  use "hrsh7th/cmp-path"
  use "hrsh7th/cmp-cmdline"
  use "hrsh7th/nvim-cmp"

  use "hrsh7th/cmp-vsnip"
  use "hrsh7th/vim-vsnip"

  -- editorconfig
  use 'editorconfig/editorconfig-vim'

  use { 'RRethy/nvim-base16' }
  use {'scalameta/nvim-metals', requires = { "nvim-lua/plenary.nvim" }}
  use({
    "jose-elias-alvarez/null-ls.nvim",
    requires = { "nvim-lua/plenary.nvim" },
  })
  use "jose-elias-alvarez/typescript.nvim"

  -- なんかいい感じに出してくれるアレ
  use {
    'nvim-telescope/telescope.nvim',
    requires = {
      { 'nvim-lua/plenary.nvim' },
      {
        'nvim-telescope/telescope-fzf-native.nvim',
        opt = true,
        run = 'make',
        disable = vim.fn.has('win64') == 1,
      },
      { 'folke/trouble.nvim' },
    },
    config = function()
      local trouble = require("trouble.providers.telescope")
      if vim.fn.has('win64') then
        require('telescope').setup {
          defaults = {
            mappings = {
              i = { ["<c-t>"] = trouble.open_with_trouble },
              n = { ["<c-t>"] = trouble.open_with_trouble },
            },
          },
        }
      else
        require('telescope').setup {
          extensions = {
            fzf = {
              fuzzy = true,                    -- false will only do exact matching
              override_generic_sorter = true,  -- override the generic sorter
              override_file_sorter = true,     -- override the file sorter
              case_mode = "smart_case",        -- or "ignore_case" or "respect_case"
                                               -- the default case_mode is "smart_case"
            }
          },
          defaults = {
            mappings = {
              i = { ["<c-t>"] = trouble.open_with_trouble },
              n = { ["<c-t>"] = trouble.open_with_trouble },
            },
          },
        }
        require('telescope').load_extension('fzf')
      end
      -- To get fzf loaded and working with telescope, you need to call
      -- load_extension, somewhere after setup function:
      vim.api.nvim_set_keymap('n', ',df', '<cmd> lua require("telescope.builtin").git_files()<CR>', { noremap = true, silent = false} )
      vim.api.nvim_set_keymap('n', ',dF', '<cmd> lua require("telescope.builtin").find_files()<CR>', { noremap = true, silent = false} )
      vim.api.nvim_set_keymap('n', ',dg', '<cmd> lua require("telescope.builtin").live_grep()<CR>', { noremap = true, silent = false} )
      vim.api.nvim_set_keymap('n', ',dG', '<cmd> lua require("telescope.builtin").grep_string()<CR>', { noremap = true, silent = false} )
      vim.api.nvim_set_keymap('n', ',db', '<cmd> lua require("telescope.builtin").buffers()<CR>', { noremap = true, silent = false} )
      vim.api.nvim_set_keymap('n', ',dt', '<cmd> lua require("telescope.builtin").help_tags()<CR>', { noremap = true, silent = false} )
      vim.api.nvim_set_keymap('n', ',dq', '<cmd> lua require("telescope.builtin").quickfix()<CR>', { noremap = true, silent = false} )
      vim.api.nvim_set_keymap('n', ',dl', '<cmd> lua require("telescope.builtin").loclist()<CR>', { noremap = true, silent = false} )
      vim.api.nvim_set_keymap('n', ',dj', '<cmd> lua require("telescope.builtin").jumplist()<CR>', { noremap = true, silent = false} )
      vim.api.nvim_set_keymap('n', ',gc', '<cmd> lua require("telescope.builtin").git_commits()<CR>', { noremap = true, silent = false} )
      vim.api.nvim_set_keymap('n', ',gs', '<cmd> lua require("telescope.builtin").git_status()<CR>', { noremap = true, silent = false} )
    end,
  }

  -- signatureを出す
  use "ray-x/lsp_signature.nvim"

  -- go用のツール
  use {
    'ray-x/go.nvim',
    --[[ disable = vim.fn.has('win64') == 1, ]]
    requires = {
      { 'nvim-treesitter/nvim-treesitter' },
      { 'ray-x/guihua.lua' },
      { 'mfussenegger/nvim-dap' },
      { 'rcarriga/nvim-dap-ui' },
      { 'theHamsta/nvim-dap-virtual-text' },
    },
  }

  -- クオートとかの囲みの便利ツール
  use 'tpope/vim-surround'

  -- diagnostic結果表示
  use {
    "folke/trouble.nvim",
    requires = "kyazdani42/nvim-web-devicons",
    config = function()
      require("trouble").setup {
        signs = {
            error = "E",
            warning = "W",
            hint = "H",
            information = "I"
        },
      }
    end
  }

  -- コメント(gccとgc, gbcとgb)
  use {
     'numToStr/Comment.nvim',
      config = function()
          require('Comment').setup()
      end
  }

  -- indent線表示(いるかこれ)
  -- use {
  --   "lukas-reineke/indent-blankline.nvim",
  --   config = function()
  --     require("indent_blankline").setup {
  --         space_char_blankline = " ",
  --         show_current_context = true,
  --         show_current_context_start = true,
  --     }
  --   end,
  -- }

  -- カッコ補完
  use "jiangmiao/auto-pairs"

  if Packer_bootstrap then
    require('packer').sync()
  end
end)
