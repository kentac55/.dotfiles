local buf_map = function(bufnr, mode, lhs, rhs, opts)
    vim.api.nvim_buf_set_keymap(bufnr, mode, lhs, rhs, opts or {
        silent = true,
    })
end

local lsp_formatting = function(bufnr)
  vim.lsp.buf.format({
    -- filter = function(clients)
    --   -- filter out clients that you don't want to use
    --   return vim.tbl_filter(function(client)
    --     return client.name ~= "tsserver"
    --   end, clients)
    -- end,
    bufnr = bufnr,
  })
end

local augroup = vim.api.nvim_create_augroup("LspFormatting", {})

local on_attach = function(client, bufnr)
  local function buf_set_keymap(...) vim.api.nvim_buf_set_keymap(bufnr, ...) end
  local function buf_set_option(...) vim.api.nvim_buf_set_option(bufnr, ...) end
  -- Enable completion triggered by <c-x><c-o>
  buf_set_option('omnifunc', 'v:lua.vim.lsp.omnifunc')

  -- Mappings.
  local opts = { noremap=true, silent=false }

  -- See `:help vim.lsp.*` for documentation on any of the below functions
  local prefix = ',s'
  buf_set_keymap('n', prefix .. 'D', '<cmd>lua vim.lsp.buf.declaration()<CR>', opts)
  buf_set_keymap('n', prefix ..'d', '<cmd>lua vim.lsp.buf.definition()<CR>', opts)
  buf_set_keymap('n', prefix .. 's', '<cmd>lua vim.lsp.buf.hover()<CR>', opts)
  buf_set_keymap('n', prefix .. 'i', '<cmd>lua vim.lsp.buf.implementation()<CR>', opts)
  buf_set_keymap('n', prefix .. 'h', '<cmd>lua vim.lsp.buf.signature_help()<CR>', opts)
  buf_set_keymap('n', prefix .. 'wa', '<cmd>lua vim.lsp.buf.add_workspace_folder()<CR>', opts)
  buf_set_keymap('n', prefix .. 'wr', '<cmd>lua vim.lsp.buf.remove_workspace_folder()<CR>', opts)
  buf_set_keymap('n', prefix .. 'ws', '<cmd>lua print(vim.inspect(vim.lsp.buf.list_workspace_folders()))<CR>', opts)
  buf_set_keymap('n', prefix .. 't', '<cmd>lua vim.lsp.buf.type_definition()<CR>', opts)
  buf_set_keymap('n', prefix .. 'R', '<cmd>lua vim.lsp.buf.rename()<CR>', opts)
  buf_set_keymap('n', prefix .. 'a', '<cmd>lua vim.lsp.buf.code_action()<CR>', opts)
  buf_set_keymap('n', prefix .. 'r', '<cmd>lua vim.lsp.buf.references()<CR>', opts)
  buf_set_keymap('n', '<C-c>', '<cmd>lua vim.diagnostic.open_float()<CR>', opts)
  buf_set_keymap('n', '<C-p>', '<cmd>lua vim.diagnostic.goto_prev()<CR>', opts)
  buf_set_keymap('n', '<C-n>', '<cmd>lua vim.diagnostic.goto_next()<CR>', opts)
  buf_set_keymap('n', prefix .. 'l', '<cmd>lua vim.diagnostic.setloclist()<CR>', opts)
  buf_set_keymap('n', prefix .. 'F', '<cmd>lua vim.lsp.buf.formatting(nil, 5000)<CR>', opts)
  require "lsp_signature".on_attach()
  vim.cmd("command! LspDef lua vim.lsp.buf.definition()")
  -- vim.cmd("command! LspFormatting lua vim.lsp.buf.formatting(nil, 5000)")
  vim.cmd("command! LspCodeAction lua vim.lsp.buf.code_action()")
  vim.cmd("command! LspHover lua vim.lsp.buf.hover()")
  vim.cmd("command! LspRename lua vim.lsp.buf.rename()")
  vim.cmd("command! LspRefs lua vim.lsp.buf.references()")
  vim.cmd("command! LspTypeDef lua vim.lsp.buf.type_definition()")
  vim.cmd("command! LspImplementation lua vim.lsp.buf.implementation()")
  vim.cmd("command! LspDiagPrev lua vim.diagnostic.goto_prev()")
  vim.cmd("command! LspDiagNext lua vim.diagnostic.goto_next()")
  vim.cmd("command! LspDiagLine lua vim.diagnostic.open_float()")
  vim.cmd("command! LspSignatureHelp lua vim.lsp.buf.signature_help()")
  -- if client.resolved_capabilities.document_formatting then
  --     vim.cmd("autocmd BufWritePre <buffer> lua vim.lsp.buf.formatting_sync(nil, 5000)")
  -- end
  if client.supports_method("textDocument/formatting") then
    vim.api.nvim_clear_autocmds({ group = augroup, buffer = bufnr })
    vim.api.nvim_create_autocmd("BufWritePre", {
      group = augroup,
      buffer = bufnr,
      callback = function()
        lsp_formatting(bufnr)
      end,
    })
  end
end

local lspconfig = require("lspconfig")
local lsp_installer = require("nvim-lsp-installer")
local capabilities = require("cmp_nvim_lsp").default_capabilities(vim.lsp.protocol.make_client_capabilities())

function Metals_init()
  local metals = require("metals")
  local metals_config = metals.bare_config()
  metals_config.init_options.statusBarProvider = "on"
  metals_config.on_attach = on_attach
  metals_config.capabilities = capabilities
  metals.initialize_or_attach(metals_config)
  require "lsp_signature".setup()
end

function Go_init()
  local path = require 'nvim-lsp-installer.core.path'
  local install_root_dir = path.concat {vim.fn.stdpath 'data', 'lsp_servers'}
  require('go').setup({
    gopls_cmd = {install_root_dir .. '/go/gopls'},
    lsp_on_attach = on_attach,
    fillstruct = 'gopls',
    dap_debug = true,
    dap_debug_gui = true
  })
  local lsp_installer_servers = require'nvim-lsp-installer.servers'

  local server_available, requested_server = lsp_installer_servers.get_server("gopls")
  if server_available then
      requested_server:on_ready(function ()
          local opts = require'go.lsp'.config() -- config() return the go.nvim gopls setup
          requested_server:setup(opts)
      end)
      if not requested_server:is_installed() then
          -- Queue the server to be installed
          requested_server:install()
      end
  end
end


local typescript = require("typescript")
typescript.setup({
    disable_commands = false, -- prevent the plugin from creating Vim commands
    debug = false, -- enable debug logging for commands
    server = { -- pass options to lspconfig's setup method
        on_attach = on_attach,
    },
})

local null_ls = require("null-ls")
null_ls.setup({
  sources = {
    null_ls.builtins.diagnostics.eslint_d,
    -- null_ls.builtins.code_actions.eslint_d,
    null_ls.builtins.formatting.prettier
  },
  on_attach = on_attach,
})

-- autocmd BufWritePre (InsertLeave?) <buffer> lua vim.lsp.buf.formatting_sync(nil, 5000)
-- autocmd BufWritePre <buffer> lua vim.lsp.buf.formatting_sync(nil, 5000)
vim.cmd([[
  augroup lsp
    au!
    au FileType java,scala,sbt lua Metals_init()
    au FileType go lua Go_init()
    autocmd BufWritePre *.go :silent! lua require('go.format').goimport()
  augroup end
]])
