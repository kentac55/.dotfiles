-- Automatically generated packer.nvim plugin loader code

if vim.api.nvim_call_function('has', {'nvim-0.5'}) ~= 1 then
  vim.api.nvim_command('echohl WarningMsg | echom "Invalid Neovim version for packer.nvim! | echohl None"')
  return
end

vim.api.nvim_command('packadd packer.nvim')

local no_errors, error_msg = pcall(function()

_G._packer = _G._packer or {}
_G._packer.inside_compile = true

local time
local profile_info
local should_profile = false
if should_profile then
  local hrtime = vim.loop.hrtime
  profile_info = {}
  time = function(chunk, start)
    if start then
      profile_info[chunk] = hrtime()
    else
      profile_info[chunk] = (hrtime() - profile_info[chunk]) / 1e6
    end
  end
else
  time = function(chunk, start) end
end

local function save_profiles(threshold)
  local sorted_times = {}
  for chunk_name, time_taken in pairs(profile_info) do
    sorted_times[#sorted_times + 1] = {chunk_name, time_taken}
  end
  table.sort(sorted_times, function(a, b) return a[2] > b[2] end)
  local results = {}
  for i, elem in ipairs(sorted_times) do
    if not threshold or threshold and elem[2] > threshold then
      results[i] = elem[1] .. ' took ' .. elem[2] .. 'ms'
    end
  end
  if threshold then
    table.insert(results, '(Only showing plugins that took longer than ' .. threshold .. ' ms ' .. 'to load)')
  end

  _G._packer.profile_output = results
end

time([[Luarocks path setup]], true)
local package_path_str = "C:\\Users\\kenta\\AppData\\Local\\Temp\\nvim\\packer_hererocks\\2.1.0-beta3\\share\\lua\\5.1\\?.lua;C:\\Users\\kenta\\AppData\\Local\\Temp\\nvim\\packer_hererocks\\2.1.0-beta3\\share\\lua\\5.1\\?\\init.lua;C:\\Users\\kenta\\AppData\\Local\\Temp\\nvim\\packer_hererocks\\2.1.0-beta3\\lib\\luarocks\\rocks-5.1\\?.lua;C:\\Users\\kenta\\AppData\\Local\\Temp\\nvim\\packer_hererocks\\2.1.0-beta3\\lib\\luarocks\\rocks-5.1\\?\\init.lua"
local install_cpath_pattern = "C:\\Users\\kenta\\AppData\\Local\\Temp\\nvim\\packer_hererocks\\2.1.0-beta3\\lib\\lua\\5.1\\?.so"
if not string.find(package.path, package_path_str, 1, true) then
  package.path = package.path .. ';' .. package_path_str
end

if not string.find(package.cpath, install_cpath_pattern, 1, true) then
  package.cpath = package.cpath .. ';' .. install_cpath_pattern
end

time([[Luarocks path setup]], false)
time([[try_loadstring definition]], true)
local function try_loadstring(s, component, name)
  local success, result = pcall(loadstring(s), name, _G.packer_plugins[name])
  if not success then
    vim.schedule(function()
      vim.api.nvim_notify('packer.nvim: Error running ' .. component .. ' for ' .. name .. ': ' .. result, vim.log.levels.ERROR, {})
    end)
  end
  return result
end

time([[try_loadstring definition]], false)
time([[Defining packer_plugins]], true)
_G.packer_plugins = {
  ["Comment.nvim"] = {
    config = { "\27LJ\2\n5\0\0\3\0\3\0\0066\0\0\0'\2\1\0B\0\2\0029\0\2\0B\0\1\1K\0\1\0\nsetup\fComment\frequire\0" },
    loaded = true,
    path = "C:\\Users\\kenta\\AppData\\Local\\nvim-data\\site\\pack\\packer\\start\\Comment.nvim",
    url = "https://github.com/numToStr/Comment.nvim"
  },
  ale = {
    commands = { "ALEEnable" },
    config = { "\27LJ\2\n-\0\0\3\0\3\0\0056\0\0\0009\0\1\0'\2\2\0B\0\2\1K\0\1\0\14ALEEnable\bcmd\bvim\0" },
    loaded = false,
    needs_bufread = true,
    only_cond = false,
    path = "C:\\Users\\kenta\\AppData\\Local\\nvim-data\\site\\pack\\packer\\opt\\ale",
    url = "https://github.com/w0rp/ale"
  },
  ["auto-pairs"] = {
    loaded = true,
    path = "C:\\Users\\kenta\\AppData\\Local\\nvim-data\\site\\pack\\packer\\start\\auto-pairs",
    url = "https://github.com/jiangmiao/auto-pairs"
  },
  ["cmp-buffer"] = {
    loaded = true,
    path = "C:\\Users\\kenta\\AppData\\Local\\nvim-data\\site\\pack\\packer\\start\\cmp-buffer",
    url = "https://github.com/hrsh7th/cmp-buffer"
  },
  ["cmp-cmdline"] = {
    loaded = true,
    path = "C:\\Users\\kenta\\AppData\\Local\\nvim-data\\site\\pack\\packer\\start\\cmp-cmdline",
    url = "https://github.com/hrsh7th/cmp-cmdline"
  },
  ["cmp-nvim-lsp"] = {
    loaded = true,
    path = "C:\\Users\\kenta\\AppData\\Local\\nvim-data\\site\\pack\\packer\\start\\cmp-nvim-lsp",
    url = "https://github.com/hrsh7th/cmp-nvim-lsp"
  },
  ["cmp-path"] = {
    loaded = true,
    path = "C:\\Users\\kenta\\AppData\\Local\\nvim-data\\site\\pack\\packer\\start\\cmp-path",
    url = "https://github.com/hrsh7th/cmp-path"
  },
  ["cmp-vsnip"] = {
    loaded = true,
    path = "C:\\Users\\kenta\\AppData\\Local\\nvim-data\\site\\pack\\packer\\start\\cmp-vsnip",
    url = "https://github.com/hrsh7th/cmp-vsnip"
  },
  ["editorconfig-vim"] = {
    loaded = true,
    path = "C:\\Users\\kenta\\AppData\\Local\\nvim-data\\site\\pack\\packer\\start\\editorconfig-vim",
    url = "https://github.com/editorconfig/editorconfig-vim"
  },
  firenvim = {
    loaded = true,
    path = "C:\\Users\\kenta\\AppData\\Local\\nvim-data\\site\\pack\\packer\\start\\firenvim",
    url = "https://github.com/glacambre/firenvim"
  },
  ["galaxyline.nvim"] = {
    config = { "\27LJ\2\n)\0\0\3\0\2\0\0046\0\0\0'\2\1\0B\0\2\1K\0\1\0\14spaceline\frequire\0" },
    loaded = true,
    path = "C:\\Users\\kenta\\AppData\\Local\\nvim-data\\site\\pack\\packer\\start\\galaxyline.nvim",
    url = "https://github.com/NTBBloodbath/galaxyline.nvim"
  },
  ["gitsigns.nvim"] = {
    config = { "\27LJ\2\n6\0\0\3\0\3\0\0066\0\0\0'\2\1\0B\0\2\0029\0\2\0B\0\1\1K\0\1\0\nsetup\rgitsigns\frequire\0" },
    loaded = true,
    path = "C:\\Users\\kenta\\AppData\\Local\\nvim-data\\site\\pack\\packer\\start\\gitsigns.nvim",
    url = "https://github.com/lewis6991/gitsigns.nvim"
  },
  ["go.nvim"] = {
    loaded = true,
    path = "C:\\Users\\kenta\\AppData\\Local\\nvim-data\\site\\pack\\packer\\start\\go.nvim",
    url = "https://github.com/ray-x/go.nvim"
  },
  ["guihua.lua"] = {
    loaded = true,
    path = "C:\\Users\\kenta\\AppData\\Local\\nvim-data\\site\\pack\\packer\\start\\guihua.lua",
    url = "https://github.com/ray-x/guihua.lua"
  },
  ["lsp_signature.nvim"] = {
    loaded = true,
    path = "C:\\Users\\kenta\\AppData\\Local\\nvim-data\\site\\pack\\packer\\start\\lsp_signature.nvim",
    url = "https://github.com/ray-x/lsp_signature.nvim"
  },
  ["markdown-preview.nvim"] = {
    commands = { "MarkdownPreview" },
    loaded = false,
    needs_bufread = false,
    only_cond = false,
    path = "C:\\Users\\kenta\\AppData\\Local\\nvim-data\\site\\pack\\packer\\opt\\markdown-preview.nvim",
    url = "https://github.com/iamcco/markdown-preview.nvim"
  },
  ["null-ls.nvim"] = {
    loaded = true,
    path = "C:\\Users\\kenta\\AppData\\Local\\nvim-data\\site\\pack\\packer\\start\\null-ls.nvim",
    url = "https://github.com/jose-elias-alvarez/null-ls.nvim"
  },
  ["nvim-base16"] = {
    loaded = true,
    path = "C:\\Users\\kenta\\AppData\\Local\\nvim-data\\site\\pack\\packer\\start\\nvim-base16",
    url = "https://github.com/RRethy/nvim-base16"
  },
  ["nvim-cmp"] = {
    loaded = true,
    path = "C:\\Users\\kenta\\AppData\\Local\\nvim-data\\site\\pack\\packer\\start\\nvim-cmp",
    url = "https://github.com/hrsh7th/nvim-cmp"
  },
  ["nvim-dap"] = {
    loaded = true,
    path = "C:\\Users\\kenta\\AppData\\Local\\nvim-data\\site\\pack\\packer\\start\\nvim-dap",
    url = "https://github.com/mfussenegger/nvim-dap"
  },
  ["nvim-dap-ui"] = {
    loaded = true,
    path = "C:\\Users\\kenta\\AppData\\Local\\nvim-data\\site\\pack\\packer\\start\\nvim-dap-ui",
    url = "https://github.com/rcarriga/nvim-dap-ui"
  },
  ["nvim-dap-virtual-text"] = {
    loaded = true,
    path = "C:\\Users\\kenta\\AppData\\Local\\nvim-data\\site\\pack\\packer\\start\\nvim-dap-virtual-text",
    url = "https://github.com/theHamsta/nvim-dap-virtual-text"
  },
  ["nvim-lsp-installer"] = {
    loaded = true,
    path = "C:\\Users\\kenta\\AppData\\Local\\nvim-data\\site\\pack\\packer\\start\\nvim-lsp-installer",
    url = "https://github.com/williamboman/nvim-lsp-installer"
  },
  ["nvim-lspconfig"] = {
    loaded = true,
    path = "C:\\Users\\kenta\\AppData\\Local\\nvim-data\\site\\pack\\packer\\start\\nvim-lspconfig",
    url = "https://github.com/neovim/nvim-lspconfig"
  },
  ["nvim-metals"] = {
    loaded = true,
    path = "C:\\Users\\kenta\\AppData\\Local\\nvim-data\\site\\pack\\packer\\start\\nvim-metals",
    url = "https://github.com/scalameta/nvim-metals"
  },
  ["nvim-treesitter"] = {
    config = { "\27LJ\2\n�\1\0\0\5\0\n\0\r6\0\0\0'\2\1\0B\0\2\0029\0\2\0005\2\3\0005\3\4\0=\3\5\0025\3\6\0005\4\a\0=\4\b\3=\3\t\2B\0\2\1K\0\1\0\14highlight\fdisable\1\3\0\0\6c\trust\1\0\2\venable\2&additional_vim_regex_highlighting\1\19ignore_install\1\2\0\0\15javascript\1\0\1\17sync_install\1\nsetup\28nvim-treesitter.configs\frequire\0" },
    loaded = true,
    path = "C:\\Users\\kenta\\AppData\\Local\\nvim-data\\site\\pack\\packer\\start\\nvim-treesitter",
    url = "https://github.com/nvim-treesitter/nvim-treesitter"
  },
  ["nvim-web-devicons"] = {
    loaded = true,
    path = "C:\\Users\\kenta\\AppData\\Local\\nvim-data\\site\\pack\\packer\\start\\nvim-web-devicons",
    url = "https://github.com/kyazdani42/nvim-web-devicons"
  },
  ["packer.nvim"] = {
    loaded = true,
    path = "C:\\Users\\kenta\\AppData\\Local\\nvim-data\\site\\pack\\packer\\start\\packer.nvim",
    url = "https://github.com/wbthomason/packer.nvim"
  },
  ["plenary.nvim"] = {
    loaded = true,
    path = "C:\\Users\\kenta\\AppData\\Local\\nvim-data\\site\\pack\\packer\\start\\plenary.nvim",
    url = "https://github.com/nvim-lua/plenary.nvim"
  },
  ["telescope.nvim"] = {
    config = { "\27LJ\2\n�\r\0\0\b\0@\0�\0016\0\0\0'\2\1\0B\0\2\0026\1\2\0009\1\3\0019\1\4\1'\3\5\0B\1\2\2\15\0\1\0X\2\19�6\1\0\0'\3\6\0B\1\2\0029\1\a\0015\3\17\0005\4\15\0005\5\v\0005\6\t\0009\a\b\0=\a\n\6=\6\f\0055\6\r\0009\a\b\0=\a\n\6=\6\14\5=\5\16\4=\4\18\3B\1\2\1X\1\28�6\1\0\0'\3\6\0B\1\2\0029\1\a\0015\3\22\0005\4\20\0005\5\19\0=\5\21\4=\4\23\0035\4\27\0005\5\25\0005\6\24\0009\a\b\0=\a\n\6=\6\f\0055\6\26\0009\a\b\0=\a\n\6=\6\14\5=\5\16\4=\4\18\3B\1\2\0016\1\0\0'\3\6\0B\1\2\0029\1\28\1'\3\21\0B\1\2\0016\1\2\0009\1\29\0019\1\30\1'\3\14\0'\4\31\0'\5 \0005\6!\0B\1\5\0016\1\2\0009\1\29\0019\1\30\1'\3\14\0'\4\"\0'\5#\0005\6$\0B\1\5\0016\1\2\0009\1\29\0019\1\30\1'\3\14\0'\4%\0'\5&\0005\6'\0B\1\5\0016\1\2\0009\1\29\0019\1\30\1'\3\14\0'\4(\0'\5)\0005\6*\0B\1\5\0016\1\2\0009\1\29\0019\1\30\1'\3\14\0'\4+\0'\5,\0005\6-\0B\1\5\0016\1\2\0009\1\29\0019\1\30\1'\3\14\0'\4.\0'\5/\0005\0060\0B\1\5\0016\1\2\0009\1\29\0019\1\30\1'\3\14\0'\0041\0'\0052\0005\0063\0B\1\5\0016\1\2\0009\1\29\0019\1\30\1'\3\14\0'\0044\0'\0055\0005\0066\0B\1\5\0016\1\2\0009\1\29\0019\1\30\1'\3\14\0'\0047\0'\0058\0005\0069\0B\1\5\0016\1\2\0009\1\29\0019\1\30\1'\3\14\0'\4:\0'\5;\0005\6<\0B\1\5\0016\1\2\0009\1\29\0019\1\30\1'\3\14\0'\4=\0'\5>\0005\6?\0B\1\5\1K\0\1\0\1\0\2\fnoremap\2\vsilent\1<<cmd> lua require(\"telescope.builtin\").git_status()<CR>\b,gs\1\0\2\fnoremap\2\vsilent\1=<cmd> lua require(\"telescope.builtin\").git_commits()<CR>\b,gc\1\0\2\fnoremap\2\vsilent\1:<cmd> lua require(\"telescope.builtin\").jumplist()<CR>\b,dj\1\0\2\fnoremap\2\vsilent\0019<cmd> lua require(\"telescope.builtin\").loclist()<CR>\b,dl\1\0\2\fnoremap\2\vsilent\1:<cmd> lua require(\"telescope.builtin\").quickfix()<CR>\b,dq\1\0\2\fnoremap\2\vsilent\1;<cmd> lua require(\"telescope.builtin\").help_tags()<CR>\b,dt\1\0\2\fnoremap\2\vsilent\0019<cmd> lua require(\"telescope.builtin\").buffers()<CR>\b,db\1\0\2\fnoremap\2\vsilent\1=<cmd> lua require(\"telescope.builtin\").grep_string()<CR>\b,dG\1\0\2\fnoremap\2\vsilent\1;<cmd> lua require(\"telescope.builtin\").live_grep()<CR>\b,dg\1\0\2\fnoremap\2\vsilent\1<<cmd> lua require(\"telescope.builtin\").find_files()<CR>\b,dF\1\0\2\fnoremap\2\vsilent\1;<cmd> lua require(\"telescope.builtin\").git_files()<CR>\b,df\20nvim_set_keymap\bapi\19load_extension\1\0\0\1\0\0\1\0\0\1\0\0\15extensions\1\0\0\bfzf\1\0\0\1\0\4\28override_generic_sorter\2\nfuzzy\2\14case_mode\15smart_case\25override_file_sorter\2\rdefaults\1\0\0\rmappings\1\0\0\6n\1\0\0\6i\1\0\0\n<c-t>\1\0\0\22open_with_trouble\nsetup\14telescope\nwin64\bhas\afn\bvim trouble.providers.telescope\frequire\0" },
    loaded = true,
    path = "C:\\Users\\kenta\\AppData\\Local\\nvim-data\\site\\pack\\packer\\start\\telescope.nvim",
    url = "https://github.com/nvim-telescope/telescope.nvim"
  },
  ["trouble.nvim"] = {
    config = { "\27LJ\2\nt\0\0\4\0\6\0\t6\0\0\0'\2\1\0B\0\2\0029\0\2\0005\2\4\0005\3\3\0=\3\5\2B\0\2\1K\0\1\0\nsigns\1\0\0\1\0\4\fwarning\6W\thint\6H\nerror\6E\16information\6I\nsetup\ftrouble\frequire\0" },
    loaded = true,
    path = "C:\\Users\\kenta\\AppData\\Local\\nvim-data\\site\\pack\\packer\\start\\trouble.nvim",
    url = "https://github.com/folke/trouble.nvim"
  },
  ["typescript.nvim"] = {
    loaded = true,
    path = "C:\\Users\\kenta\\AppData\\Local\\nvim-data\\site\\pack\\packer\\start\\typescript.nvim",
    url = "https://github.com/jose-elias-alvarez/typescript.nvim"
  },
  ["vim-matchup"] = {
    after_files = { "C:\\Users\\kenta\\AppData\\Local\\nvim-data\\site\\pack\\packer\\opt\\vim-matchup\\after\\plugin\\matchit.vim" },
    loaded = false,
    needs_bufread = true,
    only_cond = false,
    path = "C:\\Users\\kenta\\AppData\\Local\\nvim-data\\site\\pack\\packer\\opt\\vim-matchup",
    url = "https://github.com/andymass/vim-matchup"
  },
  ["vim-surround"] = {
    loaded = true,
    path = "C:\\Users\\kenta\\AppData\\Local\\nvim-data\\site\\pack\\packer\\start\\vim-surround",
    url = "https://github.com/tpope/vim-surround"
  },
  ["vim-vsnip"] = {
    loaded = true,
    path = "C:\\Users\\kenta\\AppData\\Local\\nvim-data\\site\\pack\\packer\\start\\vim-vsnip",
    url = "https://github.com/hrsh7th/vim-vsnip"
  }
}

time([[Defining packer_plugins]], false)
-- Config for: gitsigns.nvim
time([[Config for gitsigns.nvim]], true)
try_loadstring("\27LJ\2\n6\0\0\3\0\3\0\0066\0\0\0'\2\1\0B\0\2\0029\0\2\0B\0\1\1K\0\1\0\nsetup\rgitsigns\frequire\0", "config", "gitsigns.nvim")
time([[Config for gitsigns.nvim]], false)
-- Config for: telescope.nvim
time([[Config for telescope.nvim]], true)
try_loadstring("\27LJ\2\n�\r\0\0\b\0@\0�\0016\0\0\0'\2\1\0B\0\2\0026\1\2\0009\1\3\0019\1\4\1'\3\5\0B\1\2\2\15\0\1\0X\2\19�6\1\0\0'\3\6\0B\1\2\0029\1\a\0015\3\17\0005\4\15\0005\5\v\0005\6\t\0009\a\b\0=\a\n\6=\6\f\0055\6\r\0009\a\b\0=\a\n\6=\6\14\5=\5\16\4=\4\18\3B\1\2\1X\1\28�6\1\0\0'\3\6\0B\1\2\0029\1\a\0015\3\22\0005\4\20\0005\5\19\0=\5\21\4=\4\23\0035\4\27\0005\5\25\0005\6\24\0009\a\b\0=\a\n\6=\6\f\0055\6\26\0009\a\b\0=\a\n\6=\6\14\5=\5\16\4=\4\18\3B\1\2\0016\1\0\0'\3\6\0B\1\2\0029\1\28\1'\3\21\0B\1\2\0016\1\2\0009\1\29\0019\1\30\1'\3\14\0'\4\31\0'\5 \0005\6!\0B\1\5\0016\1\2\0009\1\29\0019\1\30\1'\3\14\0'\4\"\0'\5#\0005\6$\0B\1\5\0016\1\2\0009\1\29\0019\1\30\1'\3\14\0'\4%\0'\5&\0005\6'\0B\1\5\0016\1\2\0009\1\29\0019\1\30\1'\3\14\0'\4(\0'\5)\0005\6*\0B\1\5\0016\1\2\0009\1\29\0019\1\30\1'\3\14\0'\4+\0'\5,\0005\6-\0B\1\5\0016\1\2\0009\1\29\0019\1\30\1'\3\14\0'\4.\0'\5/\0005\0060\0B\1\5\0016\1\2\0009\1\29\0019\1\30\1'\3\14\0'\0041\0'\0052\0005\0063\0B\1\5\0016\1\2\0009\1\29\0019\1\30\1'\3\14\0'\0044\0'\0055\0005\0066\0B\1\5\0016\1\2\0009\1\29\0019\1\30\1'\3\14\0'\0047\0'\0058\0005\0069\0B\1\5\0016\1\2\0009\1\29\0019\1\30\1'\3\14\0'\4:\0'\5;\0005\6<\0B\1\5\0016\1\2\0009\1\29\0019\1\30\1'\3\14\0'\4=\0'\5>\0005\6?\0B\1\5\1K\0\1\0\1\0\2\fnoremap\2\vsilent\1<<cmd> lua require(\"telescope.builtin\").git_status()<CR>\b,gs\1\0\2\fnoremap\2\vsilent\1=<cmd> lua require(\"telescope.builtin\").git_commits()<CR>\b,gc\1\0\2\fnoremap\2\vsilent\1:<cmd> lua require(\"telescope.builtin\").jumplist()<CR>\b,dj\1\0\2\fnoremap\2\vsilent\0019<cmd> lua require(\"telescope.builtin\").loclist()<CR>\b,dl\1\0\2\fnoremap\2\vsilent\1:<cmd> lua require(\"telescope.builtin\").quickfix()<CR>\b,dq\1\0\2\fnoremap\2\vsilent\1;<cmd> lua require(\"telescope.builtin\").help_tags()<CR>\b,dt\1\0\2\fnoremap\2\vsilent\0019<cmd> lua require(\"telescope.builtin\").buffers()<CR>\b,db\1\0\2\fnoremap\2\vsilent\1=<cmd> lua require(\"telescope.builtin\").grep_string()<CR>\b,dG\1\0\2\fnoremap\2\vsilent\1;<cmd> lua require(\"telescope.builtin\").live_grep()<CR>\b,dg\1\0\2\fnoremap\2\vsilent\1<<cmd> lua require(\"telescope.builtin\").find_files()<CR>\b,dF\1\0\2\fnoremap\2\vsilent\1;<cmd> lua require(\"telescope.builtin\").git_files()<CR>\b,df\20nvim_set_keymap\bapi\19load_extension\1\0\0\1\0\0\1\0\0\1\0\0\15extensions\1\0\0\bfzf\1\0\0\1\0\4\28override_generic_sorter\2\nfuzzy\2\14case_mode\15smart_case\25override_file_sorter\2\rdefaults\1\0\0\rmappings\1\0\0\6n\1\0\0\6i\1\0\0\n<c-t>\1\0\0\22open_with_trouble\nsetup\14telescope\nwin64\bhas\afn\bvim trouble.providers.telescope\frequire\0", "config", "telescope.nvim")
time([[Config for telescope.nvim]], false)
-- Config for: Comment.nvim
time([[Config for Comment.nvim]], true)
try_loadstring("\27LJ\2\n5\0\0\3\0\3\0\0066\0\0\0'\2\1\0B\0\2\0029\0\2\0B\0\1\1K\0\1\0\nsetup\fComment\frequire\0", "config", "Comment.nvim")
time([[Config for Comment.nvim]], false)
-- Config for: galaxyline.nvim
time([[Config for galaxyline.nvim]], true)
try_loadstring("\27LJ\2\n)\0\0\3\0\2\0\0046\0\0\0'\2\1\0B\0\2\1K\0\1\0\14spaceline\frequire\0", "config", "galaxyline.nvim")
time([[Config for galaxyline.nvim]], false)
-- Config for: nvim-treesitter
time([[Config for nvim-treesitter]], true)
try_loadstring("\27LJ\2\n�\1\0\0\5\0\n\0\r6\0\0\0'\2\1\0B\0\2\0029\0\2\0005\2\3\0005\3\4\0=\3\5\0025\3\6\0005\4\a\0=\4\b\3=\3\t\2B\0\2\1K\0\1\0\14highlight\fdisable\1\3\0\0\6c\trust\1\0\2\venable\2&additional_vim_regex_highlighting\1\19ignore_install\1\2\0\0\15javascript\1\0\1\17sync_install\1\nsetup\28nvim-treesitter.configs\frequire\0", "config", "nvim-treesitter")
time([[Config for nvim-treesitter]], false)
-- Config for: trouble.nvim
time([[Config for trouble.nvim]], true)
try_loadstring("\27LJ\2\nt\0\0\4\0\6\0\t6\0\0\0'\2\1\0B\0\2\0029\0\2\0005\2\4\0005\3\3\0=\3\5\2B\0\2\1K\0\1\0\nsigns\1\0\0\1\0\4\fwarning\6W\thint\6H\nerror\6E\16information\6I\nsetup\ftrouble\frequire\0", "config", "trouble.nvim")
time([[Config for trouble.nvim]], false)

-- Command lazy-loads
time([[Defining lazy-load commands]], true)
pcall(vim.api.nvim_create_user_command, 'MarkdownPreview', function(cmdargs)
          require('packer.load')({'markdown-preview.nvim'}, { cmd = 'MarkdownPreview', l1 = cmdargs.line1, l2 = cmdargs.line2, bang = cmdargs.bang, args = cmdargs.args, mods = cmdargs.mods }, _G.packer_plugins)
        end,
        {nargs = '*', range = true, bang = true, complete = function()
          require('packer.load')({'markdown-preview.nvim'}, {}, _G.packer_plugins)
          return vim.fn.getcompletion('MarkdownPreview ', 'cmdline')
      end})
pcall(vim.api.nvim_create_user_command, 'ALEEnable', function(cmdargs)
          require('packer.load')({'ale'}, { cmd = 'ALEEnable', l1 = cmdargs.line1, l2 = cmdargs.line2, bang = cmdargs.bang, args = cmdargs.args, mods = cmdargs.mods }, _G.packer_plugins)
        end,
        {nargs = '*', range = true, bang = true, complete = function()
          require('packer.load')({'ale'}, {}, _G.packer_plugins)
          return vim.fn.getcompletion('ALEEnable ', 'cmdline')
      end})
time([[Defining lazy-load commands]], false)

vim.cmd [[augroup packer_load_aucmds]]
vim.cmd [[au!]]
  -- Filetype lazy-loads
time([[Defining lazy-load filetype autocommands]], true)
vim.cmd [[au FileType bash ++once lua require("packer.load")({'ale'}, { ft = "bash" }, _G.packer_plugins)]]
vim.cmd [[au FileType cpp ++once lua require("packer.load")({'ale'}, { ft = "cpp" }, _G.packer_plugins)]]
vim.cmd [[au FileType cmake ++once lua require("packer.load")({'ale'}, { ft = "cmake" }, _G.packer_plugins)]]
vim.cmd [[au FileType html ++once lua require("packer.load")({'ale'}, { ft = "html" }, _G.packer_plugins)]]
vim.cmd [[au FileType markdown ++once lua require("packer.load")({'ale'}, { ft = "markdown" }, _G.packer_plugins)]]
vim.cmd [[au FileType racket ++once lua require("packer.load")({'ale'}, { ft = "racket" }, _G.packer_plugins)]]
vim.cmd [[au FileType tex ++once lua require("packer.load")({'ale'}, { ft = "tex" }, _G.packer_plugins)]]
vim.cmd [[au FileType go ++once lua require("packer.load")({'ale'}, { ft = "go" }, _G.packer_plugins)]]
vim.cmd [[au FileType vim ++once lua require("packer.load")({'ale'}, { ft = "vim" }, _G.packer_plugins)]]
vim.cmd [[au FileType c ++once lua require("packer.load")({'ale'}, { ft = "c" }, _G.packer_plugins)]]
vim.cmd [[au FileType sh ++once lua require("packer.load")({'ale'}, { ft = "sh" }, _G.packer_plugins)]]
vim.cmd [[au FileType zsh ++once lua require("packer.load")({'ale'}, { ft = "zsh" }, _G.packer_plugins)]]
time([[Defining lazy-load filetype autocommands]], false)
  -- Event lazy-loads
time([[Defining lazy-load event autocommands]], true)
vim.cmd [[au VimEnter * ++once lua require("packer.load")({'vim-matchup'}, { event = "VimEnter *" }, _G.packer_plugins)]]
time([[Defining lazy-load event autocommands]], false)
vim.cmd("augroup END")

_G._packer.inside_compile = false
if _G._packer.needs_bufread == true then
  vim.cmd("doautocmd BufRead")
end
_G._packer.needs_bufread = false

if should_profile then save_profiles() end

end)

if not no_errors then
  error_msg = error_msg:gsub('"', '\\"')
  vim.api.nvim_command('echohl ErrorMsg | echom "Error in packer_compiled: '..error_msg..'" | echom "Please check your config for correctness" | echohl None')
end
