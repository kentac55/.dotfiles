umask 022
limit coredumpsize 0
bindkey -d

# tmux起動条件
# - tmuxが起動していない
# - linuxのGUI環境である or osxである
# - Editorの内蔵terminalではない
if [ -z "$TMUX" ] && ([ -n "$DISPLAY" ] || [ "$X_MACHINE" = "Mac" ]) && [ -z "$EDITOR_TERM" ]; then
  exec tmux -2 -u new-session
fi

# GUI起動条件
# - tty1である
# - startxが存在する
if [ "$(tty)" = "/dev/tty1" ] && type "startx" > /dev/null 2>&1; then
  exec startx
fi

. ~/.zplug/init.zsh

#if ! zplug check --verbose; then
#  printf "Install? [y/N]: "
#  if read -q; then
#      echo; zplug install
#  fi
#  echo
#fi

zplug load

for i in ~/.zshrc.d/<->_*.zsh; do
  if [ -r $i ]; then
    . $i
  fi
done
unset i

if [ -f ~/.zshrc.local ]; then
  . ~/.zshrc.local
fi

REPORTTIME=5
HISTFILE=$HOME/.zsh_history

[ -f "${XDG_CONFIG_HOME:-$HOME/.config}"/fzf/fzf.zsh ] && source "${XDG_CONFIG_HOME:-$HOME/.config}"/fzf/fzf.zsh

# if (which zprof > /dev/null 2>&1) ;then
#  zprof
# fi
eval "$(starship init zsh)"
