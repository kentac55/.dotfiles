#!/bin/bash

# set -eux

cd $(dirname $0)
PROGNAME=$(basename $0)
DOTFILES=$PWD
XDG_CONFIG_BASE=$DOTFILES/.config
XDG_CONFIG_HOME=$HOME/.config
LIBRARY="$HOME/Library/Application Support"

usage_exit() {
  echo "Usage: $PROGNAME [-h|-c|-g|-r]" 1>&2
  echo
  echo "Options:"
  echo "  -h  show this text"
  echo "  -c  for cli environment"
  echo "  -g  for gui environment(incl cli)"
  echo "  -r  for rdp environment(incl gui)"
  echo
  exit 1
}

if [ "$(id -u)" = "0" ]; then
  echo "must run under non-root user"
  exit 1
fi

MODE=""

while getopts cgrhx OPT
do
  case $OPT in
    h) usage_exit
      ;;
    \?) usage_exit
      ;;
    c) MODE="CLI"
      ;;
    g) MODE="GUI"
      ;;
    r) MODE="RDP"
      ;;
    x) MODE="CODE"
      ;;
  esac
done

echo $MODE

cli_install() {
  _cli_dep_install
  _cli_settings
}

_cli_dep_install() {
  echo "install required packages and update system-wide"
  source $PWD/packages.sh
  clidep=(
    "${alts_mac[@]}"
    "${utils_mac[@]}"
    "${devs_mac[@]}"
  )
  brew install $(IFS=' '; echo "${clidep[*]}")
  brew upgrade

  echo "install diff-highlight"
  ln -sf /usr/local/share/git-core/contrib/diff-highlight/diff-highlight $HOME/bin/diff-highlight

  echo "install prettyping"
  rm -rf $HOME/bin/prettyping
  wget https://raw.githubusercontent.com/denilsonsa/prettyping/master/prettyping -O $HOME/bin/prettyping
  chmod +x $HOME/bin/prettyping

  echo "make bin dir..."
  cd $HOME
  mkdir -p $HOME/bin

  echo "install gibo..."
  curl -L https://raw.github.com/simonwhitaker/gibo/master/gibo -so $HOME/bin/gibo
  chmod +x $HOME/bin/gibo
  gibo update
}

_cli_settings() {
  mkdir -p $HOME/.config
  ln -sf $DOTFILES/.editorconfig $HOME
  if [ -d $HOME/.fzf/.git ]; then
    cd $HOME/.fzf
    git pull
    cd $DOTFILES
  else
    rm -rf $HOME/.fzf
    git clone --depth 1 https://github.com/junegunn/fzf $HOME/.fzf
  fi
  $HOME/.fzf/install --all --xdg

  ln -sf $DOTFILES/.gitconfig $HOME
  ln -sf $DOTFILES/.gitconfig.identity.default $HOME
  ln -sf $DOTFILES/.gitignore $HOME
  ln -sf $DOTFILES/.tigrc $HOME
  ln -sf $DOTFILES/.gitmessage $HOME

  ln -sf $DOTFILES/.vimrc $HOME

  mkdir -p $XDG_CONFIG_HOME/nvim
  ln -sf $XDG_CONFIG_BASE/nvim/init.vim $XDG_CONFIG_HOME/nvim
  ln -sf $XDG_CONFIG_BASE/nvim/dein.toml $XDG_CONFIG_HOME/nvim
  ln -sf $XDG_CONFIG_BASE/nvim/dein_lazy.toml $XDG_CONFIG_HOME/nvim
  ln -sf $XDG_CONFIG_BASE/nvim/coc-settings.json $XDG_CONFIG_HOME/nvim

  if [ -d $HOME/.tmux/plugins/tpm/.git ]; then
    cd $HOME/.tmux/plugins/tpm
    git pull
    cd $DOTFILES
  else
    rm -rf $HOME/.tmux/plugins/tpm
    mkdir -p $HOME/.tmux/plugins
    git clone --depth 1 https://github.com/tmux-plugins/tpm $HOME/.tmux/plugins/tpm
  fi
  ln -sf $DOTFILES/.tmux.conf $HOME

  ln -sf $XDG_CONFIG_BASE/starship.toml $XDG_CONFIG_HOME/starship.toml

  ln -sf $DOTFILES/.zshrc $HOME
  ln -sf $DOTFILES/.zprofile $HOME
  ln -sf $DOTFILES/.zshenv $HOME
  ln -sf $DOTFILES/.zshrc.d $HOME
  if [ -d $HOME/.zplug/.git ]; then
    cd $HOME/.zplug
    git pull
    cd $DOTFILES
  else
    rm -rf $HOME/.zplug
    git clone --depth 1 https://github.com/zplug/zplug $HOME/.zplug
  fi
  X_INSTALL=1 zsh -c "source ~/.zshrc; zplug install; zplug update"

  ln -sf $DOTFILES/.gemrc $HOME

  mkdir -p $HOME/.ssh
  ln -sf $DOTFILES/.ssh/config.linux $HOME/.ssh/config

  echo "prepare pyenv..."
  if [ -d $HOME/.pyenv/.git ]; then
    cd $HOME/.pyenv
    git pull
    cd $DOTFILES
  else
    rm -rf $HOME/.pyenv
    git clone --depth 1 https://github.com/pyenv/pyenv.git $HOME/.pyenv
  fi

  echo "prepare pyenv-virtualenv"
  if [ -d $HOME/.pyenv/plugins/pyenv-virtualenv/.git ]; then
    cd $HOME/.pyenv/plugins/pyenv-virtualenv
    git pull
    cd $DOTFILES
  else
    rm -rf $HOME/.pyenv/plugins/pyenv-virtualenv
    git clone --depth 1 https://github.com/pyenv/pyenv-virtualenv.git $HOME/.pyenv/plugins/pyenv-virtualenv
  fi

  echo "fetch python version"
  export PYENV_ROOT="$HOME/.pyenv"
  eval "$($HOME/.pyenv/bin/pyenv init -)"
  pyver3=$($HOME/.pyenv/bin/pyenv install --list | ggrep -P "^  3.\d+.\d+$" | tail -n 1 | sed -r -e "s/  //g")

  echo "install python $pyver3"
  PYTHON_CONFIGURE_OPTS="--enable-framework" $HOME/.pyenv/bin/pyenv install -s $pyver3
  $HOME/.pyenv/bin/pyenv global $pyver3
  $HOME/.pyenv/bin/pyenv virtualenv $pyver3 py3nvim
  $HOME/.pyenv/bin/pyenv activate py3nvim
  pip install pynvim
  $HOME/.pyenv/bin/pyenv deactivate

  echo "prepare rbenv..."
  export RBENV_ROOT="$HOME/.rbenv"
  if [ -d $HOME/.rbenv/.git ]; then
    cd $HOME/.rbenv
    git pull
    cd $DOTFILES
  else
    rm -rf $HOME/.rbenv
    git clone --depth 1 https://github.com/rbenv/rbenv.git $HOME/.rbenv
  fi
  if [ -d $HOME/.rbenv/plugins/ruby-build/.git ]; then
    cd $HOME/.rbenv/plugins/ruby-build
    git pull
    cd $DOTFILES
  else
    rm -rf $HOME/.rbenv/plugins/ruby-build
    mkdir -p $HOME/.rbenv/plugins
    git clone --depth 1 https://github.com/rbenv/ruby-build.git $HOME/.rbenv/plugins/ruby-build
  fi
  eval "$($HOME/.rbenv/bin/rbenv init - --no-rehash)"
  rbver=$($HOME/.rbenv/bin/rbenv install --list | ggrep -P "^3.\d+.\d+$" | tail -n 1)

  echo "install ruby $rbver"
  $HOME/.rbenv/bin/rbenv install -s $rbver
  # $HOME/.rbenv/bin/rbenv global $rbver

  npm config set prefix=$HOME

  echo "prepare nvm..."
  if [ -d $HOME/.nvm/.git ]; then
    cd $HOME/.nvm
    git pull
    cd $DOTFILES
  else
    rm -rf $HOME/.nvm
    git clone --depth 1 https://github.com/creationix/nvm.git $HOME/.nvm
  fi
  . "$HOME/.nvm/nvm.sh"
  echo "install latest node"
  nvm install --lts
  # nvm use --lts
  nvm unload

  echo "prepare tfenv"
  export TFENV_ROOT="$HOME/.tfenv"
  if [ -d $HOME/.tfenv/.git ]; then
    cd $HOME/.tfenv
    git pull
    cd $DOTFILES
  else
    rm -rf $HOME/.tfenv
    git clone --depth 1 https://github.com/tfutils/tfenv.git ~/.tfenv
  fi
  echo "install latest terraform"
  PATH="$HOME/.tfenv/bin:$PATH" $HOME/.tfenv/bin/tfenv install latest
  PATH="$HOME/.tfenv/bin:$PATH" $HOME/.tfenv/bin/tfenv use latest

  echo "install python3 packages"
  PYTHONUSERBASE=$HOME /usr/local/bin/python3 -m pip install --upgrade --user $(IFS=' '; echo "${py3pkg[*]}")

  for pkg in ${py3bin[@]}; do
    PIPX_HOME=$HOME $HOME/bin/pipx install --force $pkg
  done
  PIPX_HOME=$HOME $HOME/bin/pipx reinstall-all

  echo "install node packages"
  npm install -g $(IFS=' '; echo "${nodepkg[*]}")
  npm update -g

  echo "place gnupg configs"
  mkdir -p $HOME/.gnupg
  ln -sf $DOTFILES/.gnupg/gpg.conf $HOME/.gnupg
  ln -sf $DOTFILES/.gnupg/gpg-agent.conf $HOME/.gnupg
}

gui_install() {
  cli_install
  _gui_dep_install
  _gui_settings
}

code_settings() {
  cmds="code code-oss code-insiders vscode"
  for cmd in ${cmds[@]}; do
    if type $cmd > /dev/null 2>&1; then
      _code_ext "$cmd"
    fi
  done
  _code_cfg
}

_code_ext() {
  echo "install extensions to $cmd"
  \cat $XDG_CONFIG_BASE/Code/extensions | while read line; do
    echo "add ${line} to ${cmd}"
    $cmd --install-extension $line --force
  done
  echo "uninstall extensions to $cmd"
  \cat $XDG_CONFIG_BASE/Code/x_extensions | while read line; do
    echo "remove ${line} from ${cmd}"
    $cmd --uninstall-extension $line --force
  done
}

_code_cfg() {
  echo "place configs"
  mkdir -p "$LIBRARY/Code/User"

  ln -sf $XDG_CONFIG_BASE/Code/User/settings.json "$LIBRARY/Code/User/settings.json"
  ln -sf $XDG_CONFIG_BASE/Code/User/keybindings.json "$LIBRARY/Code/User/keybindings.json"
  mkdir -p "$LIBRARY/Code - OSS"
  mkdir -p "$LIBRARY/Code - Insiders"
  ln -sf "$LIBRARY/Code/User" "$LIBRARY/Code - OSS/User"
  ln -sf "$LIBRARY/Code/User" "$LIBRARY/Code - Insiders/User"
}

_gui_dep_install() {
  casks=(
    # caffeine
    # docker
    # iterm2
    # rectangle
  )
  for cask in ${casks[@]}; do
    brew cask install $cask
  done
}

_gui_settings() {
  echo "place .ideavimrc"
  ln -sf $DOTFILES/.vimrc $HOME/.ideavimrc
  code_settings

  echo "place alacritty config"
  mkdir -p $XDG_CONFIG_HOME/alacritty
  ln -sf $XDG_CONFIG_BASE/alacritty/alacritty.yml $XDG_CONFIG_HOME/alacritty
}

if [ "$MODE" = "CLI" ]; then
  cli_install
elif [ "$MODE" = "GUI" ]; then
  gui_install
elif [ "$MODE" = "RDP" ]; then
  rdp_install
elif [ "$MODE" = "CODE" ]; then
  code_settings
else
  usage_exit
fi
